# Hastur #

This is the repository Hastur, a rest api for a file system manager to read information about factions information for a DnD mmorpg

## Requirements ##

### Config File ###
Make sure to copy config_dist.json to config.json and fill with the necessary values. The following are numerical values, so you need to remove the quotes and put a number there: crypt_json jwt_expiration

### Dependencies ###
There a couple of dependencies you would nee to `go get <package>` before building the project: golang.org/x/crypto/bcrypt github.com/dgrijalva/jwt-go

## Endpoints ##
### Protected endpoints ###
There are some endpoints that are protected under JWT validation. This JWT needs to be sent as a header under the name **Token**. Expect a **401** http status code in case the token was not validated successfully.

### 500 http status code ###
If something unexpected happens while processing your request, expect a **500** http status code as a response.

### /healthz ###
Make a **GET** request to this endpoint to check the healthiness of the application, this implies DB connection check as well.

### /login ###
Make a  **POST** request to this endpoint to obtain the JWT needed for validation for the protected endpoints. This requieres the following body in **JSON** format
```json
{
    "user": "sampleuser",
    "password": "password"
}
```
This will return the following body in case the login was succesfull alonside a **200** http status code
```json
{
    "message": "Login succesful",
    "token": "JWTTOKEN"
}
```
In case the password and the user is do not match, expect a **401** http status code as aresponse, this also handles the case of a missing.

### /signup ###
Make a **POST** request to add a user for verification for later use for JWT validation. This requieres the following body in **JSON** format
```json
{
    "user": "sampleuser",
    "password": "password"
}
```
This will return a ***200** http status code as a repsonse if the user was registered succesfully, however if the user already exists it will return a **400** http status code, adding a user won't give that user admin or dev privileges, that is something that will need to be done manually.

### /factions ###
Make a **GET** request to this endpoint to obtain all of the factions that are currently in the database, this will return a **200** http status code along side the following body.
```json
[
    {
        "id": 1,
        "name": "Astrologers",
        "cap": 10
    },
    {
        "id": 2,
        "name": "Merchant's Guild",
        "cap": 10
    },
    {
        "id": 3,
        "name": "Mage's experiments",
        "cap": 12
    }
]
```
If there is an error however, it will return a **500** http status code along side the body.

### /faction ###
#### GET ####
Make a **GET** request to this endpoint to obtain all of the factions that are currently in the database that have the filters that are available in the query params. The query params will have the following priority: ID > Name and can be utilized in the following manner.
`https://url:1234/faction?id=1&name=astrologers`
And it will return the following body alonside a **200** http status code, please note that the id needs to be an integer and the name needs to be exact and is case sensitive.
```json
{
    "id": 1,
    "name": "Astrologers",
    "cap": 10
}
```
If the paramters are not valid, like in the example of a string in the id, it will return a **500** http status code. In the case of there not being any matches for the search it will return a **404** http status code.

#### POST ####
Make a **POST** request to this endpoint to add a faction to the database, this will requiere the JWT token validation mentioned previously, this request requieres the following JSON body in the request.
```json
{
    "name": "Teca Trailblazers",
    "cap": 10
}
```
If the post was succesfull it will return a **200** http status code along side the following body.
```json
{
    "id": 4,
    "name": "Teca Trailblazers",
    "cap": 10
}
```
In case a faction of the same name already exists, the faction cap is 0 or any of the items in the body are missing, this will return a **400** http status code.

#### PATCH ####
Make a **PATCH** request to this endpoint to edit a faction that's already in the database, this will requiere the JWT token validation mentioned previously, this request requieres the following JSON body in the request. Please not that you cannot edit the id, so it will edit the faction which said id
```json
{
    "id": 4,
    "name": "Teca Trailblazers",
    "cap": 11
}
```
If the post was succesfull it will return a **200** http status code along side the following body.
```json
{
    "id": 4,
    "name": "Teca Trailblazers",
    "cap": 11
}
```
In case a faction of the same name already exists, the faction cap is 0, any of the items in the body are missing, or the faction doesn't exist, this will return a **400** http status code.

#### DELETE ####
Make a **DELETE** request to this endpoint to delete one of the factions that is currently in the database. To delete the faction the id will have to be especified in the id query param like so.
`faction?id=4`
And it will return a **200** http status code if the faction was deleted succesfully, however if there is no faction with that id it will return a **400** http status code.

### /characters ###
#### GET ####
Make a **GET** request to this endpoint to obtain one or multiple characters that are currently in the database. The filters are abailable in the query params and will have the following priority:

* By id `/characters?id=1`, this will return one character with that specific id
* By name `/characters?name=Skeleton`, this will return one character with that name, the query is not case sentitive but the name needs to be exact

The following filters are all applied at the same time but can't be applied with name and id as those are exact searches: 

* By level `/characters?level=1` this will return all of the characters with that level
* By race `/characters?race=elf` this will return all of the characters with that race in their character, this needs to be exact and is not case senstivie
* By faction `/characters?faction=astrologers` this will return all of the characters with that faction in their character, this needs to be exact and is not case senstivie 
* By class `/characters?class=Monk` this will return all of the characters with that class in their character, this is not case sensitive and doesn't need to be exact, as long as the class in the character it will be filtered.
* By region `/characters?region=Xeque` this will return all of the characters that are currently on that region, this is is not case sensitive and does need to be exact
* By Last Played `/characters?last_played` this will return the players in order of last played in ascending order.

For the searches where it returns multiple characters it will return it in the following format along side a **200** http status code.
```json
[
    {
        "id": "1",
        "name": "Schmoovin Skeleton",
        "race": "Skeleton",
        "faction": "Astrologers",
        "class": "Monk(undead)8",
        "region": "Xeque",
        "level": 8,
        "exp": 10,
        "last_played": "06-22-2021",
        "sheet": ""
    },
    {
        "id": "2",
        "name": "Skeleton",
        "race": "skeleton",
        "faction": "skeletons",
        "class": "skeleton 2",
        "region": "Klug",
        "level": 2,
        "exp": 10,
        "last_played": "04-23-2021",
        "sheet": ""
    },
    {
        "id": "10",
        "name": "Schmoovin Skeleton 2",
        "race": "skeleton",
        "faction": "skeletons",
        "class": "skeleton 2",
        "region": "Klug",
        "level": 2,
        "exp": 10,
        "last_played": "10-24-2021",
        "sheet": ""
    },
    {
        "id": "8",
        "name": "Wrightbaking",
        "race": "skeleton",
        "faction": "skeletons",
        "class": "skeleton 2",
        "region": "Klug",
        "level": 2,
        "exp": 10,
        "last_played": "01-02-2021",
        "sheet": ""
    }
]
```
In case it doesn't find any results with the filters it will return an empty array.

For the specific searches the api will return a **200** http status code along side the following body in the response.
```json
{
    "id": "1",
    "name": "Schmoovin Skeleton",
    "race": "Skeleton",
    "faction": "Astrologers",
    "class": "Monk(undead)8",
    "region": "Xeque",
    "level": 8,
    "exp": 10,
    "last_played": "06-22-2021",
    "sheet": ""
}
```
And in case the search wasn't succesfull it will return a **404** http status code.

#### POST ####
Make a **POST** request to this endpoint to add a character to the database, this request will be need to have the JWT token for verification as it's a protected endpoint and will require the following body in JSON format.
```json
{
    "id": "9",
    "name": "Pollo",
    "race": "Skeleton",
    "faction": "Astrologers",
    "class": "Wizard(necromancer)8",
    "region": "Xeque",
    "level": 8,
    "exp": 10,
    "last_played": "06-22-2021",
    "sheet": ""
}
```
This allows for bulk adding in form of a json, object array, this will return a **200** http status code along side the following body
```json
{
    "id": "9",
    "name": "Pollo",
    "race": "Skeleton",
    "faction": "Astrologers",
    "class": "Wizard(necromancer)8",
    "region": "Xeque",
    "level": 8,
    "exp": 10,
    "last_played": "06-22-2021",
    "sheet": ""
}
```
In case the a character with a name or an ID of a character that already exists it will return a **400** http status code, or in case a field is empty or isn't valid it will also return a **400** http status code, the only field that isn't requiered is the sheet field.

#### PATCH ####
Make a **PATCH** request to this endpoint to add a character to the database, this request will be need to have the JWT token for verification as it's a protected endpoint and will require the following body in JSON format.
```json
{
    "id": "9",
    "name": "Pollo",
    "race": "Skeleton",
    "faction": "Astrologers",
    "class": "Wizard(necromancer)8",
    "region": "Xeque",
    "level": 8,
    "exp": 10,
    "last_played": "06-22-2021",
    "sheet": ""
}
```
This allows for bulk adding in form of a json, object array, this will return a **200** http status code along side the following body
```json
{
    "id": "9",
    "name": "Pollo",
    "race": "Skeleton",
    "faction": "Astrologers",
    "class": "Wizard(necromancer)8",
    "region": "Xeque",
    "level": 8,
    "exp": 10,
    "last_played": "06-22-2021",
    "sheet": ""
}
```
In case the a character with a name of a character that already exists it will return a **400** http status code, along side the case if any of the characters id's doesn't exist within the database or if any of the characters have a non valid field it it will also return a **400** http status code.

#### DELETE ####
Make a **DELETE** request to this endpoint to delete a character from the database, this will requiere a JWT Token as this is a protected endpoint, to delete a character, you will need ot identify that character using the query paramater filter 
`/characters?id=9`
In case the deletion was succesfull it will returna a **200** https status, or in the case of the id being invalid or the character not existing it will return a **400** http status.

### /inventories ###
Make a **GET** request to this endpint to obtain all of the inventories that are currently in the database, you can also search multiple specific inventories with ids in the query parameters in the following matter.
`/inventories?id=1,2`
This will return a **200** http status along side the following body in the response of the request.
```json
[
    {
        "id": "1",
        "gold": 0,
        "items": [
            {
                "name": "Longsword",
                "description": "1d8 sword that is sharp",
                "quantity": 10
            },
            {
                "name": "Shield",
                "description": "+2 to AC",
                "quantity": 1
            }
        ],
        "magical_items": [
            {
                "name": "Fire King feather",
                "description": "A feather that allows you to cast fireball once, after that it is consumed",
                "quantity": 2
            }
        ],
        "bank": {
            "gold": 10,
            "items": [
                {
                    "name": "Longsword",
                    "description": "1d8 sword that is sharp",
                    "quantity": 10
                },
                {
                    "name": "Shield",
                    "description": "+2 to AC",
                    "quantity": 1
                }
            ],
            "magical_items": [
                {
                    "name": "Fire King feather",
                    "description": "A feather that allows you to cast fireball once, after that it is consumed",
                    "quantity": 2
                }
            ]
        }
    }
]
```

### /inventory ###
#### GET ####
Make a **GET** request to this endpint to obtain a specific inventory that is currently in the database using query parameters in the following matter.
`/inventories?id=1`
This will return a **200** http status along side the following body in the response of the request or a **404** in case no inventory was found.
```json
{
    "id": "1",
    "gold": 0,
    "items": [
        {
            "name": "Longsword",
            "description": "1d8 sword that is sharp",
            "quantity": 10
        },
        {
            "name": "Shield",
            "description": "+2 to AC",
            "quantity": 1
        }
    ],
    "magical_items": [
        {
            "name": "Fire King feather",
            "description": "A feather that allows you to cast fireball once, after that it is consumed",
            "quantity": 2
        }
    ],
     "bank": {
        "gold": 10,
        "items": [
            {
                "name": "Longsword",
                "description": "1d8 sword that is sharp",
                "quantity": 10
            },
            {
                "name": "Shield",
                "description": "+2 to AC",
                "quantity": 1
            }
        ],
        "magical_items": [
            {
                "name": "Fire King feather",
                "description": "A feather that allows you to cast fireball once, after that it is consumed",
                "quantity": 2
            }
        ]
    }
}
```
#### POST ####
Make a **POST** request to this endpoint to add an inventory to the database, this will requiere a JWT Token as this is a protected endpoint, this will require the id query param and an optional gold query parameter in the following matter
`/inventory?id=8&gold=10`
In case the inventory add was succesfull, it will return a **200** http status along side the following body
```json
{
    "id": "8",
    "gold": 10,
    "items": [],
    "magical_items": [],
    "bank": {}
}
```
And in case an inventory with that id already exists with that id or the id is 0 it will return a **400** http status message.
#### DELETE ####
Make a **DELETE** request to this endpoint to delete an inventory from the the database, this will require a JWT Token as this is a protected endpoint, this will require the id query parameter to indicate which id is going to be deleted, and is need in the following matter
`/inventory?id=9`
In case there is no inventory with that id it will return a **404** http status code, and in the case of a succesfull deletion it will return a **200** http status code.

### /gold ###
Make a **PATCH** request to this endpoint to update the gold of an inventory, this will require a JWT Token as this is a protected endpoint, this will require the id query parameter to indicate the id of the inventory will be updated and the gold query paramater ot indicate how much gold will it get added to that inventory in the following matter or you can indicate multiple ids using, this will update all of the inventories.
`/gold?id=8&gold=100`
In the case the request was succesful, this will return a **200** http status code along side the following body of the updated inventory.
```json
[
    {
        "id": "8",
        "gold": 110,
        "items": [],
        "magical_items": [],
        "bank": {}
    }
]
```
In case an inventory any with the id doesn't exist, or the gold ammount is 0, this will return a **400** http status code.

### /items ###
#### POST ####
Make a **POST** request to this endpoint to add items and magical items to the inventory, this will require a JWT Token as this is a protected endpoint, this will require the id query parameter ot indicate the ids of the inventories that the items will be added to, this can be done in the following manner
`/items?id=1,2,3`
This will also require a JSON body with either an array of magic items or an array of mundane items like the following.
```json
{
    "items": [
        {
            "name": "Longsword",
            "description": "Deals d8 slashing damage",
            "quantity": 1
        }
    ],
    "magical_items": [
        {
            "name": "+1 Longsword",
            "description": "Deals d8 slashing damage with a +1 bonus and deals magical damage",
            "quantity": 1
        }
    ]
}
```
In case the post is succesfull, this will return a **200** http status code, along side the following body in the response, in the case that an inventory with that id doesn't exist, the id is empty or is 0, or both of the items b magical items arrays are empty, the api will return a **400** http status code.
```json
{
    "id": "8",
    "gold": 110,
    "items": [
        {
            "name": "Longsword",
            "description": "Deals d8 slashing damage",
            "quantity": 1
        }
    ],
    "magical_items": [
        {
            "name": "+1 Longsword",
            "description": "Deals d8 slashing damage with a +1 bonus and deals magical damage",
            "quantity": 1
        }
    ],
    "bank": {}
}
```
### PATCH ###
Make a **PATCH** request to this endpoint to add items and magical items to the inventory, this will require a JWT Token as this is a protected endpoint, this will require the id query parameter to indicate the id of the inventory that the items will be added to, this can be done in the following manner
`/items?id=1`
This will also require a JSON body with either an array of magic items or an array of mundane items like the following.
```json
{
    "items": [
        {
            "name": "Longsword",
            "description": "Deals d8 slashing damage",
            "quantity": 2
        }
    ],
    "magical_items": [
        {
            "name": "+1 Longsword",
            "description": "Deals d8 slashing damage with a +1 bonus and deals magical damage",
            "quantity": 2
        }
    ]
}
```
In case the post is succesfull, this will return a **200** http status code, along side the following body in the response, in the case that the inventory with the id doesn't exist, the id is empty or is 0, or both of the items b magical items arrays are empty, the api will return a **400** http status code.
```json
{
    "id": "8",
    "gold": 110,
    "items": [
        {
            "name": "Longsword",
            "description": "Deals d8 slashing damage",
            "quantity": 2
        }
    ],
    "magical_items": [
        {
            "name": "+1 Longsword",
            "description": "Deals d8 slashing damage with a +1 bonus and deals magical damage",
            "quantity": 2
        }
    ]
}
```
#### DELETE ####
Make a **DELETE** request to this endpoint to delete any mundane items from an specific invnetory, this will require the JWT Token as this is a protected endpoint, to indicate what items and inventory they will be delete from this will be needed to be done using query parameters in the following matter
`/items?id=8&name=Longsword,Shortsword`
In case, either one of the itmes is not in the inventory, there are no items, or the id is invalid, this will return a **400** http status code. In the case the request is succesful this will return a **200** http stats code along side the following body in the repsonse.
```json
{
    "id": "8",
    "gold": 110,
    "items": [],
    "magical_items": [
        {
            "name": "+1 Longsword",
            "description": "Deals d8 slashing damage with a +1 bonus and deals magical damage",
            "quantity": 2
        }
    ],
    "bank": {}
}
```

### /magic_items ###
Make a **DELETE** request to this endpoint to delete any mundane items from an specific invnetory, this will require the JWT Token as this is a protected endpoint, to indicate what items and inventory they will be delete from this will be needed to be done using query parameters in the following matter
`/items?id=8&name=1 Longsword,Shortsword`
In case, either one of the itmes is not in the inventory, there are no items, or the id is invalid, this will return a **400** http status code. In the case the request is succesful this will return a **200** http stats code along side the following body in the repsonse.
```json
{
    "id": "8",
    "gold": 110,
    "items": [],
    "magical_items": []
}
```

### /bank_items ###
Make a **PATCH** request to this endpoint to move items from a specific inventory to the bank field of that same inventory, this will require the JWT Token as this is a protected endpoint, to indicate what items will be moved to the bank field using a json body with an array of items, the id of the inventory and if it's being transfered from the bank to the inventory of viseversa as following
```json
{
    "id": "8",
    "items": [
        {
            "name": "Longsword",
            "description": "Deals d8 slashing damage",
            "quantity": 1
        }
    ],
    "to_bank": true
}
```
And it will return an inventory with the updated bank and inventory
```json
{
    "id": "8",
    "gold": 110,
    "items": [],
    "magical_items": [
        {
            "name": "+1 Longsword",
            "description": "Deals d8 slashing damage with a +1 bonus and deals magical damage",
            "quantity": 2
        }
    ],
    "bank": {
        "gold": 0,
        "items": [
            {
                "name": "Longsword",
                "description": "Deals d8 slashing damage",
                "quantity": 1
            }
        ],
        "magical_items": []
    } 
}
```

### /bank_magic_items ###
Make a **PATCH** request to this endpoint to move items from a specific inventory to the bank field of that same inventory, this will require the JWT Token as this is a protected endpoint, to indicate what items will be moved to the bank field using a json body with an array of items, the id of the inventory and if it's being transfered from the bank to the inventory of viseversa as following
```json
{
    "id": "8",
    "items": [
        {
            "name": "+1 Longsword",
            "description": "Deals d8 slashing damage with a +1 bonus and deals magical damage",
            "quantity": 1
        }
    ],
    "to_bank": true
}
```
And it will return an inventory with the updated bank and inventory
```json
{
    "id": "8",
    "gold": 110,
    "items": [],
    "magical_items": [
        {
            "name": "+1 Longsword",
            "description": "Deals d8 slashing damage with a +1 bonus and deals magical damage",
            "quantity": 1
        }
    ],
    "bank": {
        "gold": 0,
        "items": [
            {
                "name": "Longsword",
                "description": "Deals d8 slashing damage",
                "quantity": 1
            }
        ],
        "magical_items": [
            {
                "name": "+1 Longsword",
                "description": "Deals d8 slashing damage with a +1 bonus and deals magical damage",
                "quantity": 1
            }
        ]
    } 
}
```

### /dtds ###

#### GET ####
Make a **GET** request to this endpoint to obtain the downtime days along the id of a specific character currenlty in the databas. To make this request you need ot use the `/dtds?id=1` filter to indicate what character we're receving the info from.

During a successful search it will return in the following format along side a ***200** http status code
```json
{
    "id": "1",
    "dtds": 7,
}
```
In teh case that there isn't any character with that id it will return a **404** http status code.

#### PATCH ####
Make a **PATCH** request to this endpoint to obtain update the downtime days for a specific character currently in the database along side the number of days being used to substract from the downtime days using the following format: `/dtds?id=1&dtds=1`. During a seccesfull patch it will return in hte following format along side a **200** http status code.
```json
{
    "id": "1",
    "dtds": 6,
}
```
In teh case that there isn't any character with that id it will return a **404** http status code.

#### POST ####
Make a **POST** request to this endpoint to reset to the ammount of downtime days on all of the characters, with a default of 7 days to reset, however we can change the number of the the down time days being reset by utsing the *dtds* argument as following `/dtds?dtds=8`, and it will return a **200** http status code along side a message indicating the downtime days have been reset.

### /exp ###

#### GET ####
Make a **GET** request to this endpoint to obtain the exp and the level along the id of a specific character currenlty in the databas. To make this request you need ot use the `/exp?id=1` filter to indicate what character we're receving the info from.

During a successful search it will return in the following format along side a ***200** http status code
```json
{
    "id": "1",
    "exp": 7,
    "level": 2
}
```
In teh case that there isn't any character with that id it will return a **404** http status code.

#### PATCH ####
Make a **PATCH** request to this endpoint to obtain update the exp for a specific character currently in the database along side the number of exp being used to add from the downtime days using the following format: `/exp?id=1&exp=1`. During a seccesfull patch it will return in hte following format along side a **200** http status code.
```json
{
    "id": "1",
    "exp": 8,
    "level": 2
}
```
In teh case that there isn't any character with that id it will return a **404** http status code.

## General Information ##

### Contribution guidelines ###

* Everything must be unit tested
* Follow the format set by current project
* Pull requests for this will be approved by the owner of the repository

### Contact information ###
In case of needing to contact, please do so via twitter or Discord
* Grizzly ~ ☆#0645
* @TheGrzzly