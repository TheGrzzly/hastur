package main

import (
	"log"
	"net/http"
	"os"

	"hastur/config"
	"hastur/controller"
	"hastur/middleware"
	"hastur/repository"
	"hastur/router"
	"hastur/service/crypt"
	"hastur/service/database"
	"hastur/service/jwt"
	"hastur/usecase"

	"github.com/gorilla/handlers"
	"github.com/unrolled/render"
)

func main() {
	args := os.Args
	httpsEnabled := true
	if len(args) > 0 {
		if args[1] == "http" {
			httpsEnabled = false
		}
	}

	renderer := render.New()

	appConfig, err := config.LoadFromConfigFile()
	if err != nil {
		log.Fatalln("Error retreiving the app configuration", err)
	}

	db, err := database.New(appConfig)
	if err != nil {
		log.Fatalln("Error creating session to AWS", err)
	}

	healthChecker := controller.HealthChecker{
		db,
	}

	if err = healthChecker.IsHealthy(); err != nil {
		log.Fatalln("error checking deps of healthiness: ", err)
	}

	if err = db.CreateTables(); err != nil {
		log.Fatalln("Error creating the tables for the database: ", err)
	}

	repository := repository.New()

	cryptService := crypt.New(appConfig)
	jwtService := jwt.New(appConfig)

	jwtMiddleware := middleware.New(jwtService, renderer)

	userUsecase := usecase.NewUser(repository, cryptService, jwtService)
	factionUsecase := usecase.NewFaction(db)
	characterUsecase := usecase.NewCharacter(db)
	inventoryUsecase := usecase.NewInventory(db)

	statusController := controller.NewStatus(renderer, healthChecker)
	userController := controller.NewUser(renderer, userUsecase)
	factionController := controller.NewFaction(renderer, factionUsecase)
	characterController := controller.NewCharacter(renderer, characterUsecase)
	inventoryController := controller.NewInventory(renderer, inventoryUsecase)

	router := router.GetRouter(statusController, userController, factionController, characterController, inventoryController, jwtMiddleware)

	log.Println("Starting API server in port 1234")

	if httpsEnabled {
		log.Fatal(http.ListenAndServeTLS(":1234", "certificate.crt", "private.key", handlers.CORS(
			handlers.AllowedHeaders(
				[]string{"X-Requested-With", "Content-Type", "Authorization"},
			),
			handlers.AllowedMethods(
				[]string{"GET", "POST", "PATCH", "DELETE", "HEAD", "OPTIONS"},
			),
			handlers.AllowedOrigins(
				[]string{"*"},
			))(router),
		))
	} else {
		log.Fatal(http.ListenAndServe(":1234", handlers.CORS(
			handlers.AllowedHeaders(
				[]string{"X-Requested-With", "Content-Type", "Authorization"},
			),
			handlers.AllowedMethods(
				[]string{"GET", "POST", "PATCH", "DELETE", "HEAD", "OPTIONS"},
			),
			handlers.AllowedOrigins(
				[]string{"*"},
			))(router),
		))
	}
}
