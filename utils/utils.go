package utils

import (
	"hastur/model"
	"log"
	"strings"
	"time"

	cons "hastur/constant"
)

const timeFormat = "01-02-2006"

func SortByLastPlayed(characters []model.Character) []model.Character {
	dates := []time.Time{}
	n := len(characters)

	for _, character := range characters {
		t, err := time.Parse(timeFormat, character.LastPlayed)
		if err != nil {
			log.Println(cons.ErrorParsingDateLayout, err)
			t = time.Now()
		}
		dates = append(dates, t)
	}

	swapped := true

	for swapped {
		swapped = false

		for i := 0; i < n-1; i++ {
			if dates[i].After(dates[i+1]) {

				dates[i], dates[i+1] = dates[i+1], dates[i]
				characters[i], characters[i+1] = characters[i+1], characters[i]

				swapped = true
			}
		}
	}

	return characters
}

func ItemsArrayIntoMap(items []model.Item) map[string]model.Item {
	itemMap := map[string]model.Item{}

	for _, i := range items {
		itemMap[strings.ToLower(i.Name)] = i
	}

	return itemMap
}

func ValidateItemsInMap(items []model.Item, m map[string]model.Item) bool {
	for _, i := range items {
		if val, ok := m[strings.ToLower(i.Name)]; ok {
			if val.Quantity >= i.Quantity {
				continue
			}
		}
		return false
	}

	return true
}

func ValidateValidDate(date string) bool {
	_, err := time.Parse(timeFormat, date)
	return err == nil
}

func ValidateDates(dates ...string) bool {
	for _, date := range dates {
		if !ValidateValidDate(date) {
			return false
		}
	}
	return true
}
