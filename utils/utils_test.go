package utils

import (
	"hastur/model"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateDate(t *testing.T) {
	testCases := []struct {
		desc  string
		date  string
		valid bool
	}{
		{
			desc:  "Valid date",
			date:  "09-22-2021",
			valid: true,
		},
		{
			desc:  "Non-Valid date",
			date:  "09/20/2021",
			valid: false,
		},
	}

	for _, tc := range testCases {
		assert.Equal(t, tc.valid, ValidateValidDate(tc.date))
	}
}

func TestOrderCharacters(t *testing.T) {
	testCases := []struct {
		desc             string
		characters       []model.Character
		sortedCharacters []model.Character
	}{
		{
			desc:             "Empty array",
			characters:       []model.Character{},
			sortedCharacters: []model.Character{},
		},
		{
			desc: "Array with 2 players sorted",
			characters: []model.Character{
				{
					ID:         "1",
					LastPlayed: "10-22-2021",
				},
				{
					ID:         "2",
					LastPlayed: "12-22-2021",
				},
			},
			sortedCharacters: []model.Character{
				{
					ID:         "1",
					LastPlayed: "10-22-2021",
				},
				{
					ID:         "2",
					LastPlayed: "12-22-2021",
				},
			},
		},
		{
			desc: "Array with 2 players unsorted",
			characters: []model.Character{
				{
					ID:         "1",
					LastPlayed: "12-22-2021",
				},
				{
					ID:         "2",
					LastPlayed: "12-20-2021",
				},
			},
			sortedCharacters: []model.Character{
				{
					ID:         "2",
					LastPlayed: "12-20-2021",
				},
				{
					ID:         "1",
					LastPlayed: "12-22-2021",
				},
			},
		},
		{
			desc: "Character array with no day",
			characters: []model.Character{
				{
					ID:         "4",
					LastPlayed: "",
				},
				{
					ID:         "2",
					LastPlayed: "01-20-2021",
				},
			},
			sortedCharacters: []model.Character{
				{
					ID:         "2",
					LastPlayed: "01-20-2021",
				},
				{
					ID:         "4",
					LastPlayed: "",
				},
			},
		},
	}

	for _, tc := range testCases {
		assert.Equal(t, tc.sortedCharacters, SortByLastPlayed(tc.characters))
	}
}
