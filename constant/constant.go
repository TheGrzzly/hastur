package constant

const (
	ErrorParsingDateLayout      = "Error occured parsing the layout of date"
	ErrorParsingToken           = "Error parsing token"
	ErrorParsingQueryParameters = "Error parsing query paramters"
	ErrorParsingBodyParamters   = "Error parsing body parameters"
	InternalServerError         = "Error occured internally"
	ErrorGettingQueryParams     = "Error getting query params"
	TokenIsntValid              = "Token Isn't Valid"

	ErrorReadingFactions      = "Something went wrong when reading the factions"
	EnterParameterForSearch   = "Please enter parameter for the search"
	FactionNotFound           = "Faction not found"
	InvalidFaction            = "Invalid faction"
	FactionAlreadyExists      = "Faction already exists"
	ErrorPostingFactions      = "Somethign went wrong when posting the factions"
	FactionDoesntExist        = "Faction doesn't exist"
	FactionWithNameExists     = "Faction with the same name already exists"
	FactionDeletedSuccesfully = "Faction deleted succesfully"

	ErrorReadingCharacters      = "Something went wrong when reading the characters"
	CharacterNotFound           = "Character not found"
	CharacterBadRequest         = "Bad request, all character must have all of the fields"
	CharacterAlreadyExists      = "Character already exists"
	CharacterNotFoundBadRequest = "Bad request, Character not found"
	CharacterWithNameExists     = "Character with new name alrady exists"
	InvalidCharacter            = "Invalid Character"
	CharacterDoesntExist        = "Character does not exist"
	CharacterDeletedSuccesfully = "Character deleted succesfully"

	NotEnoughDowntimeDays = "Character does not have enough downtime days"
	DownTimeDaysReset     = "Downtime days have reset for all the players"

	UserAlreadyExists    = "User Already Exists"
	UserSignupSuccess    = "User registered succesfully"
	PasswordNotMatch     = "User and password does not match"
	LogingSuccess        = "Login succesful"
	ErrorGeneratingToken = "Error Generating token"

	BadRequestSent               = "Bad request sent"
	InventoryWithIdAlreadyExists = "Inventory with the id that was provided already exists"
	InventoryNotFound            = "Inventory Not found"
	InventoryDeletedSuccess      = "Inventory was deleted succesfully"
)
