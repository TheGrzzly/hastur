package model

type Character struct {
	ID                  string `json:"id"`
	Name                string `json:"name"`
	Race                string `json:"race"`
	Faction             string `json:"faction"`
	Class               string `json:"class"`
	Region              string `json:"region"`
	Level               int    `json:"level"`
	Exp                 int    `json:"exp"`
	LastPlayed          string `json:"last_played"`
	DTDs                int    `json:"dtds"`
	LastExpEarnedDate   string `json:"last_exp_earned_date"`
	LastEventPartDate   string `json:"last_event_part_date"`
	LastEventExpDate    string `json:"last_event_exp_date"`
	LastRumorPartDate   string `json:"last_rumor_part_date"`
	TotalSessionsPlayed int    `json:"total_sessions_played"`
	TotalEventPart      int    `json:"total_event_part"`
	TotalRumorPart      int    `json:"total_rumor_part"`
	TotalNpcPings       int    `json:"total_npc_pings"`
	Sheet               string `json:"sheet"`
}

type CharacterParams struct {
	ReqCharacter *Character `json:"character"`
}

type CharacterQueryParams struct {
	ID         string
	Name       string
	Faction    string
	Level      int
	Race       string
	Region     string
	Class      string
	Exp        int
	DTDs       int
	LastPlayed bool
}

type ExperienceResponse struct {
	ID    string `json:"id"`
	Exp   int    `json:"exp"`
	Level int    `json:"level"`
}

type DowntimeDaysResponse struct {
	ID   string `json:"id"`
	DTDs int    `json:"dtds"`
}
