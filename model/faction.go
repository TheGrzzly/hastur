package model

type Faction struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Cap  int    `json:"cap"`
}

type FactionParams struct {
	ReqFaction *Faction `json:"faction"`
}

type FactionQueryParams struct {
	ID   int
	Name string
}
