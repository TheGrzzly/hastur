package model

type User struct {
	User     string
	Password string
	IsAdmin  bool
	IsDev    bool
}

type UserParams struct {
	User     string `json:"user"`
	Password string `json:"password"`
}
