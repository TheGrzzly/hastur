package model

type Item struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Quantity    int    `json:"quantity"`
}

type Inventory struct {
	ID           string  `json:"id"`
	Gold         float64 `json:"gold"`
	Items        []Item  `json:"items"`
	MagicalItems []Item  `json:"magical_items"`
	Bank         Bank    `json:"bank"`
}

type Bank struct {
	Gold         float64 `json:"gold"`
	Items        []Item  `json:"items"`
	MagicalItems []Item  `json:"magical_items"`
}

type InventoryParams struct {
	ID           string `json:"id"`
	Items        []Item `json:"items"`
	MagicalItems []Item `json:"magical_items"`
}

type BankingParams struct {
	ID     string `json:"id"`
	Items  []Item `json:"items"`
	ToBank bool   `json:"to_bank"`
}

type InventoryQueryParams struct {
	ID     string
	Ids    []string
	Name   string
	Names  []string
	Gold   float64
	ToBank bool
}
