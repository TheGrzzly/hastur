package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"path/filepath"
)

type App struct {
	AwsAccessKeyId     string `json:"aws_access_key_id"`
	AwsSecretAccessKey string `json:"aws_secret_access_key"`
	CryptCost          int    `json:"crypt_cost"`
	JWTKey             string `json:"jwt_key"`
	JWTExpiration      int    `json:"jwt_expiration"`
}

const (
	configFilePath = "./config.json"
)

func LoadFromConfigFile() (*App, error) {
	var app App

	configFile, err := ioutil.ReadFile(filepath.Clean(configFilePath))
	if err != nil {
		log.Println(err)
		return nil, err
	}

	err = json.Unmarshal(configFile, &app)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return &app, nil
}
