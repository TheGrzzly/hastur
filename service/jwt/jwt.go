package jwt

import (
	"fmt"
	"hastur/config"
	"time"

	cons "hastur/constant"

	jwtgo "github.com/dgrijalva/jwt-go"
)

type Service struct {
	key             []byte
	tokenExpiration time.Duration
}

func New(ac *config.App) *Service {
	duration, _ := time.ParseDuration(fmt.Sprintf("%dm", ac.JWTExpiration))

	return &Service{
		key:             []byte(ac.JWTKey),
		tokenExpiration: duration,
	}
}

func (s *Service) Generate(user string, isAdmin, devAccess bool) (string, error) {

	type myCustomClaims struct {
		User      string `json:"user"`
		IsAdmin   bool   `json:"isAdmin"`
		DevAccess bool   `json:"devAccess"`
		Exp       int64  `json:"exp"`
		jwtgo.StandardClaims
	}

	claims := myCustomClaims{
		user,
		isAdmin,
		devAccess,
		time.Now().Add(s.tokenExpiration).Unix(),
		jwtgo.StandardClaims{
			ExpiresAt: time.Now().Add(s.tokenExpiration).Unix(),
			Issuer:    "me SmileW",
		},
	}

	token := jwtgo.NewWithClaims(jwtgo.SigningMethodHS256, claims)

	return token.SignedString(s.key)
}

func (s *Service) ValidateAdmin(etoken string) bool {
	if etoken == "" {
		return false
	}

	token, err := jwtgo.Parse(etoken, func(token *jwtgo.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwtgo.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf(cons.ErrorParsingToken)
		}

		return s.key, nil
	})
	if err != nil || !token.Valid {
		return false
	}

	claims := token.Claims.(jwtgo.MapClaims)
	return claims["isAdmin"] == true
}

func (s *Service) ValidateDevAccess(etoken string) bool {
	if etoken == "" {
		return false
	}

	token, err := jwtgo.Parse(etoken, func(token *jwtgo.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwtgo.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf(cons.ErrorParsingToken)
		}

		return s.key, nil
	})
	if err != nil || !token.Valid {
		return false
	}

	claims := token.Claims.(jwtgo.MapClaims)
	return claims["devAccess"] == true
}
