package jwt

import (
	"hastur/config"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetup(t *testing.T) {
	config := config.App{
		JWTExpiration: 10,
		JWTKey:        "This is a test key",
	}

	jwt := New(&config)

	assert.NotNil(t, jwt)
}

func TestJWTGeneration(t *testing.T) {
	testCases := []struct {
		desc       string
		user       string
		isAdmin    bool
		devAccess  bool
		errorIsNil bool
	}{
		{
			desc:       "Basic JWT test",
			user:       "Test User",
			isAdmin:    true,
			devAccess:  true,
			errorIsNil: true,
		},
		{
			desc:       "Empty user test",
			errorIsNil: true,
		},
		{
			desc:       "Only Admin Access",
			user:       "Test User",
			isAdmin:    true,
			errorIsNil: true,
		},
		{
			desc:       "Dev only Access",
			user:       "Test User",
			devAccess:  true,
			errorIsNil: true,
		},
	}

	for _, tc := range testCases {
		jwt := New(&config.App{
			JWTKey:        "Test Key",
			JWTExpiration: 10,
		})

		_, err := jwt.Generate(tc.user, tc.isAdmin, tc.devAccess)
		if tc.errorIsNil {
			assert.Nil(t, err)
		} else {
			assert.NotNil(t, err)
		}
	}
}

func TestAdminValidation(t *testing.T) {
	testCases := []struct {
		desc           string
		user           string
		token          string
		generatedToken bool
		isAdmin        bool
		devAccess      bool
		isValid        bool
	}{
		{
			desc:      "Basic JWT test",
			user:      "Test User",
			isAdmin:   true,
			devAccess: true,
			isValid:   true,
		},
		{
			desc:    "Empty user test",
			isValid: false,
		},
		{
			desc:    "Only Admin Access",
			user:    "Test User",
			isAdmin: true,
			isValid: true,
		},
		{
			desc:      "Dev only Access",
			user:      "Test User",
			devAccess: true,
			isValid:   false,
		},
		{
			desc:           "Empty token",
			generatedToken: true,
		},
		{
			desc:           "Non Token as token",
			token:          "token",
			generatedToken: true,
		},
	}

	for _, tc := range testCases {
		jwt := New(&config.App{
			JWTKey:        "Test Key",
			JWTExpiration: 10,
		})

		token, _ := jwt.Generate(tc.user, tc.isAdmin, tc.devAccess)

		if tc.generatedToken {
			token = tc.token
		}

		assert.Equal(t, tc.isValid, jwt.ValidateAdmin(token))
	}
}

func TestDevValidation(t *testing.T) {
	testCases := []struct {
		desc           string
		user           string
		token          string
		generatedToken bool
		isAdmin        bool
		devAccess      bool
		isValid        bool
	}{
		{
			desc:      "Basic JWT test",
			user:      "Test User",
			isAdmin:   true,
			devAccess: true,
			isValid:   true,
		},
		{
			desc:    "Empty user test",
			isValid: false,
		},
		{
			desc:    "Only Admin Access",
			user:    "Test User",
			isAdmin: true,
			isValid: false,
		},
		{
			desc:      "Dev only Access",
			user:      "Test User",
			devAccess: true,
			isValid:   true,
		},
		{
			desc:           "Empty token",
			generatedToken: true,
		},
		{
			desc:           "Non Token as token",
			token:          "token",
			generatedToken: true,
		},
	}

	for _, tc := range testCases {
		jwt := New(&config.App{
			JWTKey:        "Test Key",
			JWTExpiration: 10,
		})

		token, _ := jwt.Generate(tc.user, tc.isAdmin, tc.devAccess)

		if tc.generatedToken {
			token = tc.token
		}

		assert.Equal(t, tc.isValid, jwt.ValidateDevAccess(token))
	}
}
