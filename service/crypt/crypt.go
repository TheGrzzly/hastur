package crypt

import (
	"hastur/config"

	"golang.org/x/crypto/bcrypt"
)

type Service struct {
	cost int
}

func New(ac *config.App) *Service {
	return &Service{
		cost: ac.CryptCost,
	}
}

func (s *Service) EncryptPassword(passwd string) string {
	bytePasswd, _ := bcrypt.GenerateFromPassword([]byte(passwd), s.cost)
	return string(bytePasswd)
}

func (s *Service) DoesPasswordsMatch(reqPasswd, dbPasswd string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(dbPasswd), []byte(reqPasswd))
	return err == nil
}
