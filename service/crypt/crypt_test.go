package crypt

import (
	"hastur/config"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetup(t *testing.T) {
	config := config.App{
		CryptCost: 10,
	}

	crypt := New(&config)

	assert.NotNil(t, crypt)
}

func TestPasswordGeneration(t *testing.T) {
	testCases := []struct {
		desc     string
		password string
		cost     int
		result   bool
	}{
		{
			desc:     "1234 password",
			password: "1234",
			cost:     14,
			result:   true,
		},
		{
			desc:     "empty password",
			password: "",
			cost:     10,
			result:   true,
		},
	}

	for _, tc := range testCases {
		crypt := New(&config.App{CryptCost: tc.cost})

		assert.Equal(t, tc.result, crypt.DoesPasswordsMatch(tc.password, crypt.EncryptPassword(tc.password)))
	}
}

func TestPasswordMatching(t *testing.T) {
	testCases := []struct {
		desc            string
		password        string
		hashed_password string
		result          bool
	}{
		{
			desc:            "1234 password correct",
			password:        "12345",
			hashed_password: "$2a$14$E39GF/8O/yltZderFmZG1.2omVXF4TcajdrocsflKWQYP5QHcldAe",
			result:          true,
		},
		{
			desc:            "1234 password incorrect",
			password:        "1235",
			hashed_password: "$2a$14$E39GF/8O/yltZderFmZG1.2omVXF4TcajdrocsflKWQYP5QHcldAe",
			result:          false,
		},
	}

	for _, tc := range testCases {
		crypt := New(&config.App{CryptCost: 10})

		assert.Equal(t, tc.result, crypt.DoesPasswordsMatch(tc.password, tc.hashed_password))
	}
}
