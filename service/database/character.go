package database

import (
	"hastur/model"
	"hastur/utils"
	"log"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

const (
	CHARACTER_TABLE_NAME = "Character"
)

func (s *Service) CreateCharacterTable() error {
	_, err := s.dynamo.CreateTable(&dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(5),
			WriteCapacityUnits: aws.Int64(5),
		},
		TableName: aws.String(CHARACTER_TABLE_NAME),
	})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeResourceInUseException:
				log.Println(aerr.Error())
			default:
				return aerr.OrigErr()
			}
		} else {
			return err
		}
	}

	return nil
}

func (s *Service) GetAllCharacters() ([]model.Character, error) {
	params := &dynamodb.ScanInput{
		TableName: aws.String(CHARACTER_TABLE_NAME),
	}

	result, err := s.dynamo.Scan(params)
	if err != nil {
		return nil, err
	}

	return toModelCharacters(result.Items)
}

func (s *Service) GetCharacterById(id string) (*model.Character, error) {
	result, err := s.dynamo.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(CHARACTER_TABLE_NAME),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeResourceNotFoundException:
				log.Println(dynamodb.ErrCodeResourceNotFoundException, aerr.Error())
				return nil, nil
			default:
				return nil, err
			}
		} else {
			log.Println("Could not get error from aws error convertion")
			return nil, err
		}
	}
	if result.Item == nil {
		return nil, nil
	}

	return toModelCharacter(result.Item)
}

func (s *Service) GetCharacterByName(name string) (*model.Character, error) {
	filt := expression.Name("name").Equal(expression.Value(name))
	proj := expression.NamesList(
		expression.Name("id"),
		expression.Name("name"),
		expression.Name("race"),
		expression.Name("faction"),
		expression.Name("class"),
		expression.Name("region"),
		expression.Name("level"),
		expression.Name("exp"),
		expression.Name("last_played"),
		expression.Name("dtds"),
		expression.Name("last_exp_earned_date"),
		expression.Name("last_event_part_date"),
		expression.Name("last_event_exp_date"),
		expression.Name("last_rumor_part_date"),
		expression.Name("total_sessions_played"),
		expression.Name("total_event_part"),
		expression.Name("total_rumor_part"),
		expression.Name("total_npc_pings"),
		expression.Name("sheet"),
	)
	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()
	if err != nil {
		return nil, err
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      expr.Projection(),
		FilterExpression:          expr.Filter(),
		TableName:                 aws.String(CHARACTER_TABLE_NAME),
	}

	result, err := s.dynamo.Scan(params)
	if err != nil {
		return nil, err
	}

	if len(result.Items) == 0 {
		return nil, nil
	}

	return toModelCharacter(result.Items[0])
}

func (s *Service) GetCharactersWithFilter(level int, race, faction, class, region string, date bool) ([]model.Character, error) {
	characters, err := s.GetAllCharacters()
	if err != nil {
		return nil, err
	}

	charactersResult := []model.Character{}
	for _, character := range characters {
		if level != 0 && level != character.Level {
			continue
		}
		if race != "" && !strings.EqualFold(race, character.Race) {
			continue
		}
		if faction != "" && !strings.EqualFold(faction, character.Faction) {
			continue
		}
		if class != "" && !strings.Contains(strings.ToLower(character.Class), strings.ToLower(class)) {
			continue
		}
		if region != "" && !strings.EqualFold(region, character.Region) {
			continue
		}
		charactersResult = append(charactersResult, character)
	}

	if date {
		charactersResult = utils.SortByLastPlayed(charactersResult)
	}

	return charactersResult, nil
}

func (s *Service) PostCharacter(character model.Character) error {
	attrval, err := dynamodbattribute.MarshalMap(character)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      attrval,
		TableName: aws.String(CHARACTER_TABLE_NAME),
	}

	_, err = s.dynamo.PutItem(input)
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) PatchCharacter(character model.Character) error {
	_, err := s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#N":   aws.String("name"),
			"#R":   aws.String("race"),
			"#F":   aws.String("faction"),
			"#C":   aws.String("class"),
			"#E":   aws.String("region"),
			"#L":   aws.String("level"),
			"#X":   aws.String("exp"),
			"#P":   aws.String("last_played"),
			"#D":   aws.String("dtds"),
			"#W":   aws.String("last_exp_earned_date"),
			"#LEP": aws.String("last_event_part_date"),
			"#LEE": aws.String("last_event_exp_date"),
			"#LRP": aws.String("last_rumor_part_date"),
			"#TS":  aws.String("total_sessions_played"),
			"#TEP": aws.String("total_event_part"),
			"#TRP": aws.String("total_rumor_part"),
			"#TNP": aws.String("total_npc_pings"),
			"#S":   aws.String("sheet"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":name": {
				S: aws.String(character.Name),
			},
			":race": {
				S: aws.String(character.Race),
			},
			":faction": {
				S: aws.String(character.Faction),
			},
			":class": {
				S: aws.String(character.Class),
			},
			":region": {
				S: aws.String(character.Region),
			},
			":level": {
				N: aws.String(strconv.Itoa(character.Level)),
			},
			":exp": {
				N: aws.String(strconv.Itoa(character.Exp)),
			},
			":last_played": {
				S: aws.String(character.LastPlayed),
			},
			":dtds": {
				N: aws.String(strconv.Itoa(character.DTDs)),
			},
			":last_exp_earned_date": {
				S: aws.String(character.LastExpEarnedDate),
			},
			":last_event_part_date": {
				S: aws.String(character.LastEventPartDate),
			},
			":last_event_exp_date": {
				S: aws.String(character.LastEventExpDate),
			},
			":last_rumor_part_date": {
				S: aws.String(character.LastRumorPartDate),
			},
			":total_sessions_played": {
				N: aws.String(strconv.Itoa(character.TotalSessionsPlayed)),
			},
			":total_event_part": {
				N: aws.String(strconv.Itoa(character.TotalEventPart)),
			},
			":total_rumor_part": {
				N: aws.String(strconv.Itoa(character.TotalRumorPart)),
			},
			":total_npc_pings": {
				N: aws.String(strconv.Itoa(character.TotalNpcPings)),
			},
			":sheet": {
				S: aws.String(character.Sheet),
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(character.ID),
			},
		},
		TableName: aws.String(CHARACTER_TABLE_NAME),
		UpdateExpression: aws.String(`
			SET #N = :name, 
			#R = :race, 
			#F = :faction, 
			#C = :class, 
			#E = :region, 
			#L = :level, 
			#X = :exp, 
			#P = :last_played, 
			#D = :dtds, 
			#W = :last_exp_earned_date, 
			#LEP = :last_event_part_date, 
			#LEE = :last_event_exp_date, 
			#LRP = :last_rumor_part_date,
			#TS = :total_sessions_played,
			#TEP = :total_event_part,
			#TRP = :total_rumor_part,
			#TNP = :total_npc_pings,
			#S = :sheet`),
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) DeleteCharacter(id string) error {
	_, err := s.dynamo.DeleteItem(&dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(CHARACTER_TABLE_NAME),
	})

	if err != nil {
		return err
	}

	return nil
}

func (s *Service) GetDowntimeDaysById(id string) (*model.DowntimeDaysResponse, error) {
	proj := expression.NamesList(
		expression.Name("id"),
		expression.Name("dtds"),
	)
	expr, err := expression.NewBuilder().WithProjection(proj).Build()
	if err != nil {
		return nil, err
	}

	result, err := s.dynamo.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(CHARACTER_TABLE_NAME),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		ExpressionAttributeNames: expr.Names(),
		ProjectionExpression:     expr.Projection(),
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeResourceNotFoundException:
				log.Println(dynamodb.ErrCodeResourceNotFoundException, aerr.Error())
				return nil, nil
			default:
				return nil, err
			}
		} else {
			log.Println("Could not get error from aws error convertion")
			return nil, err
		}
	}
	if result.Item == nil {
		return nil, nil
	}

	return toModelDowntimeDays(result.Item)
}

func (s *Service) UpdateDowntimeDays(id string, dtds int) error {
	_, err := s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#D": aws.String("dtds"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":dtds": {
				N: aws.String(strconv.Itoa(dtds)),
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		TableName:        aws.String(CHARACTER_TABLE_NAME),
		UpdateExpression: aws.String("SET #D = :dtds"),
	})

	if err != nil {
		return err
	}

	return nil
}

func (s *Service) GetExperienceById(id string) (*model.ExperienceResponse, error) {
	proj := expression.NamesList(
		expression.Name("id"),
		expression.Name("level"),
		expression.Name("exp"),
	)
	expr, err := expression.NewBuilder().WithProjection(proj).Build()
	if err != nil {
		return nil, err
	}

	result, err := s.dynamo.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(CHARACTER_TABLE_NAME),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		ExpressionAttributeNames: expr.Names(),
		ProjectionExpression:     expr.Projection(),
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeResourceNotFoundException:
				log.Println(dynamodb.ErrCodeResourceNotFoundException, aerr.Error())
				return nil, nil
			default:
				return nil, err
			}
		} else {
			log.Println("Could not get error from aws error convertion")
			return nil, err
		}
	}
	if result.Item == nil {
		return nil, nil
	}

	return toModelExperience(result.Item)
}

func (s *Service) UpdateExperience(id string, exp int) error {
	_, err := s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#E": aws.String("exp"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":exp": {
				N: aws.String(strconv.Itoa(exp)),
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		TableName:        aws.String(CHARACTER_TABLE_NAME),
		UpdateExpression: aws.String("SET #E = :exp"),
	})

	if err != nil {
		return err
	}

	return nil
}

func toModelCharacters(items []map[string]*dynamodb.AttributeValue) ([]model.Character, error) {
	characters := []model.Character{}
	for _, i := range items {
		character, err := toModelCharacter(i)
		if err != nil {
			return nil, err
		}

		characters = append(characters, *character)
	}

	return characters, nil
}

func toModelCharacter(item map[string]*dynamodb.AttributeValue) (*model.Character, error) {
	character := model.Character{}

	err := dynamodbattribute.UnmarshalMap(item, &character)
	if err != nil {
		log.Println("Error Transforming to Character Item")
		return nil, err
	}

	return &character, nil
}

func toModelDowntimeDays(item map[string]*dynamodb.AttributeValue) (*model.DowntimeDaysResponse, error) {
	downtime := model.DowntimeDaysResponse{}
	err := dynamodbattribute.UnmarshalMap(item, &downtime)
	if err != nil {
		log.Println("Error Transforming to Downtime Item")
		return nil, err
	}

	return &downtime, nil
}

func toModelExperience(item map[string]*dynamodb.AttributeValue) (*model.ExperienceResponse, error) {
	experience := model.ExperienceResponse{}
	err := dynamodbattribute.UnmarshalMap(item, &experience)
	if err != nil {
		log.Println("Error Transforming to Experience Item")
		return nil, err
	}

	return &experience, nil
}
