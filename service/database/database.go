package database

import (
	"fmt"
	"hastur/config"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// Service for database calls with dynamodb
type Service struct {
	dynamo *dynamodb.DynamoDB
}

// New Database service struct
func New(ac *config.App) (*Service, error) {
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String("us-east-1"),
		Credentials: credentials.NewStaticCredentials(ac.AwsAccessKeyId, ac.AwsSecretAccessKey, ""),
	})

	if err != nil {
		return nil, err
	}

	return &Service{
		dynamo: dynamodb.New(session.Must(sess, err)),
	}, nil
}

func (s *Service) CreateTables() error {
	if err := s.CreateFactionTable(); err != nil {
		return err
	}
	if err := s.CreateCharacterTable(); err != nil {
		return err
	}
	if err := s.CreateInventoryTable(); err != nil {
		return err
	}
	return nil
}

//isHealthy check DB connection
func (s *Service) IsHealthy() error {
	result, err := s.dynamo.ListTables(&dynamodb.ListTablesInput{})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeInternalServerError:
				log.Println(dynamodb.ErrCodeInternalServerError, aerr.Error())
			default:
				log.Println(aerr.Error())
			}
		} else {
			fmt.Println(err.Error())
		}
		return err
	}
	log.Println(result)
	return err
}
