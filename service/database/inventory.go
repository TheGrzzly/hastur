package database

import (
	"fmt"
	"hastur/model"
	"log"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

const (
	INVENTORY_TABLE_NAME = "Inventory"
)

func (s *Service) CreateInventoryTable() error {
	_, err := s.dynamo.CreateTable(&dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(5),
			WriteCapacityUnits: aws.Int64(5),
		},
		TableName: aws.String(INVENTORY_TABLE_NAME),
	})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeResourceInUseException:
				log.Println(aerr.Error())
			default:
				return aerr.OrigErr()
			}
		} else {
			return err
		}
	}

	return nil
}

func (s *Service) GetAllInventories() ([]model.Inventory, error) {
	params := &dynamodb.ScanInput{
		TableName: aws.String(INVENTORY_TABLE_NAME),
	}

	result, err := s.dynamo.Scan(params)
	if err != nil {
		return nil, err
	}

	return toModelInventories(result.Items)
}

func (s *Service) GetInventoriesByIds(ids []string) ([]model.Inventory, error) {
	keys := []map[string]*dynamodb.AttributeValue{}
	for _, id := range ids {
		keys = append(keys, map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		})
	}

	result, err := s.dynamo.BatchGetItem(&dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			INVENTORY_TABLE_NAME: {
				Keys: keys,
			},
		},
	})

	if err != nil {
		return nil, err
	}

	return toModelInventories(result.Responses[INVENTORY_TABLE_NAME])
}

func (s *Service) GetInventory(id string) (*model.Inventory, error) {
	result, err := s.dynamo.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(INVENTORY_TABLE_NAME),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeResourceNotFoundException:
				log.Println(dynamodb.ErrCodeResourceNotFoundException, aerr.Error())
				return nil, nil
			default:
				return nil, err
			}
		} else {
			log.Println("Could not get error from aws error convertion")
			return nil, err
		}
	}
	if result.Item == nil {
		return nil, nil
	}

	return toModelInventory(result.Item)
}

func (s *Service) InsertInventory(id string, gold float64) error {
	inventory := model.Inventory{
		ID:           id,
		Gold:         gold,
		Items:        []model.Item{},
		MagicalItems: []model.Item{},
		Bank:         model.Bank{},
	}
	attrval, err := dynamodbattribute.MarshalMap(inventory)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		TableName: aws.String(INVENTORY_TABLE_NAME),
		Item:      attrval,
	}

	_, err = s.dynamo.PutItem(input)
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) DeleteInventory(id string) error {
	_, err := s.dynamo.DeleteItem(&dynamodb.DeleteItemInput{
		TableName: aws.String(INVENTORY_TABLE_NAME),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
	})

	if err != nil {
		return err
	}

	return nil
}

func (s *Service) AddGoldToInventory(id string, gold float64) error {
	_, err := s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#G": aws.String("gold"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":gold": {
				N: aws.String(fmt.Sprintf("%.2f", gold)),
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		TableName:        aws.String(INVENTORY_TABLE_NAME),
		UpdateExpression: aws.String("SET #G = :gold"),
	})

	if err != nil {
		return err
	}

	return nil
}

func (s *Service) AddGoldToBank(id string, bank model.Bank) error {
	attrval, err := dynamodbattribute.MarshalMap(bank)
	if err != nil {
		return err
	}

	_, err = s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#B": aws.String("bank"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":bank": {
				M: attrval,
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		TableName:        aws.String(INVENTORY_TABLE_NAME),
		UpdateExpression: aws.String("SET #B = :bank"),
	})

	if err != nil {
		return err
	}

	return nil
}

func (s *Service) PostItems(id string, items, magic_items []model.Item, inventory model.Inventory) error {
	flag := false
	for _, item := range items {
		for i, old_item := range inventory.Items {
			if strings.EqualFold(item.Name, old_item.Name) {
				inventory.Items[i].Quantity = old_item.Quantity + item.Quantity
				flag = true
				break
			}
		}

		if !flag {
			inventory.Items = append(inventory.Items, item)
		}
		flag = false
	}

	for _, item := range magic_items {
		for i, old_item := range inventory.MagicalItems {
			if strings.EqualFold(item.Name, old_item.Name) {
				inventory.MagicalItems[i].Quantity = old_item.Quantity + item.Quantity
				flag = true
				break
			}
		}

		if !flag {
			inventory.MagicalItems = append(inventory.MagicalItems, item)
		}
		flag = false
	}

	return s.UpdateItems(id, inventory.Items, inventory.MagicalItems)
}

func (s *Service) PatchItems(id string, items, magical_items []model.Item, inventory model.Inventory) error {
	for i, item_old := range inventory.Items {
		for _, item := range items {
			if strings.EqualFold(item.Name, item_old.Name) {
				inventory.Items[i].Description = item.Description
				inventory.Items[i].Quantity = item.Quantity
				break
			}
		}
	}

	for i, item_old := range inventory.MagicalItems {
		for _, item := range magical_items {
			if strings.EqualFold(item.Name, item_old.Name) {
				inventory.MagicalItems[i].Description = item.Description
				inventory.MagicalItems[i].Quantity = item.Quantity
				break
			}
		}
	}

	return s.UpdateItems(id, inventory.Items, inventory.MagicalItems)
}

func (s *Service) GetItems(id string, items map[string]bool) ([]string, error) {
	result := []string{}

	inventory, err := s.GetInventory(id)
	if err != nil {
		return nil, err
	}

	for _, item := range inventory.Items {
		if _, ok := items[strings.ToLower(item.Name)]; ok {
			result = append(result, item.Name)
		}
	}

	return result, nil
}

func (s *Service) GetMagicItems(id string, magical_items map[string]bool) ([]string, error) {
	result := []string{}

	inventory, err := s.GetInventory(id)
	if err != nil {
		return nil, err
	}

	for _, item := range inventory.MagicalItems {
		if _, ok := magical_items[strings.ToLower(item.Name)]; ok {
			result = append(result, item.Name)
		}
	}

	return result, nil
}

func (s *Service) DeleteItems(id string, items map[string]bool, inventory []model.Item) error {
	result := []model.Item{}

	for _, item_old := range inventory {
		if _, ok := items[strings.ToLower(item_old.Name)]; !ok {
			result = append(result, item_old)
		}
	}

	itemList, err := dynamodbattribute.MarshalList(result)
	if err != nil {
		return err
	}

	_, err = s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#I": aws.String("items"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":items": {
				L: itemList,
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		TableName:        aws.String(INVENTORY_TABLE_NAME),
		UpdateExpression: aws.String("SET #I = :items"),
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) DeleteMagicItems(id string, items map[string]bool, inventory []model.Item) error {
	result := []model.Item{}

	for _, item_old := range inventory {
		if _, ok := items[strings.ToLower(item_old.Name)]; !ok {
			result = append(result, item_old)
		}
	}

	itemList, err := dynamodbattribute.MarshalList(result)
	if err != nil {
		return err
	}

	_, err = s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#M": aws.String("magical_items"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":magical_items": {
				L: itemList,
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		TableName:        aws.String(INVENTORY_TABLE_NAME),
		UpdateExpression: aws.String("SET #M = :magical_items"),
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) UpdateItems(id string, items, magical_items []model.Item) error {
	it, err := dynamodbattribute.MarshalList(items)
	if err != nil {
		return err
	}
	mi, err := dynamodbattribute.MarshalList(magical_items)
	if err != nil {
		return err
	}

	_, err = s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#I": aws.String("items"),
			"#M": aws.String("magical_items"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":items": {
				L: it,
			},
			":magical_items": {
				L: mi,
			},
		},
		TableName: aws.String(INVENTORY_TABLE_NAME),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		UpdateExpression: aws.String("SET #I = :items, #M = :magical_items"),
	})

	if err != nil {
		return err
	}

	return nil
}

func (s *Service) UpdateBankItems(id string, items []model.Item, bank model.Bank) error {
	it, err := dynamodbattribute.MarshalList(items)
	if err != nil {
		return err
	}
	ba, err := dynamodbattribute.MarshalMap(bank)
	if err != nil {
		return err
	}
	_, err = s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#I": aws.String("items"),
			"#B": aws.String("bank"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":items": {
				L: it,
			},
			":bank": {
				M: ba,
			},
		},
		TableName: aws.String(INVENTORY_TABLE_NAME),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		UpdateExpression: aws.String("SET #I = :items, #B = :bank"),
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) UpdateBankMagicItems(id string, items []model.Item, bank model.Bank) error {
	it, err := dynamodbattribute.MarshalList(items)
	if err != nil {
		return err
	}
	ba, err := dynamodbattribute.MarshalMap(bank)
	if err != nil {
		return err
	}
	_, err = s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#M": aws.String("magical_items"),
			"#B": aws.String("bank"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":magical_items": {
				L: it,
			},
			":bank": {
				M: ba,
			},
		},
		TableName: aws.String(INVENTORY_TABLE_NAME),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
		},
		UpdateExpression: aws.String("SET #M = :magical_items, #B = :bank"),
	})
	if err != nil {
		return err
	}

	return nil
}

func toModelInventories(items []map[string]*dynamodb.AttributeValue) ([]model.Inventory, error) {
	inventories := []model.Inventory{}
	for _, i := range items {
		inventory, err := toModelInventory(i)
		if err != nil {
			return nil, err
		}

		inventories = append(inventories, *inventory)
	}

	return inventories, nil
}

func toModelInventory(item map[string]*dynamodb.AttributeValue) (*model.Inventory, error) {
	inventory := model.Inventory{
		ID:           *item["id"].S,
		Items:        []model.Item{},
		MagicalItems: []model.Item{},
	}
	gold, err := strconv.ParseFloat(*item["gold"].N, 64)
	if err != nil {
		log.Println("Error Transforming to gold to float")
		return nil, err
	}
	inventory.Gold = gold

	for _, i := range item["items"].L {
		log.Println(i.M)
		invItem, err := toModelItem(i.M)
		if err != nil {
			return nil, err
		}

		inventory.Items = append(inventory.Items, *invItem)
	}

	for _, i := range item["magical_items"].L {
		log.Println(i.M)
		magic_item, err := toModelItem(i.M)
		if err != nil {
			return nil, err
		}

		inventory.MagicalItems = append(inventory.MagicalItems, *magic_item)
	}

	bank, err := toModelBank(item["bank"].M)
	if err != nil {
		log.Println(err)

		return nil, err
	}
	inventory.Bank = *bank

	return &inventory, nil
}

func toModelItem(item map[string]*dynamodb.AttributeValue) (*model.Item, error) {
	inventoryItem := model.Item{}

	err := dynamodbattribute.UnmarshalMap(item, &inventoryItem)
	if err != nil {
		return nil, err
	}

	return &inventoryItem, nil
}

func toModelBank(bank map[string]*dynamodb.AttributeValue) (*model.Bank, error) {
	bankItem := model.Bank{
		Items:        []model.Item{},
		MagicalItems: []model.Item{},
	}
	gold, err := strconv.ParseFloat(*bank["gold"].N, 64)
	if err != nil {
		log.Println("Error transforming gold to float")
		return nil, err
	}
	bankItem.Gold = gold

	for _, i := range bank["items"].L {
		log.Println(i.M)
		invItem, err := toModelItem(i.M)
		if err != nil {
			return nil, err
		}

		bankItem.Items = append(bankItem.Items, *invItem)
	}

	for _, i := range bank["magical_items"].L {
		log.Println(i.M)
		magic_item, err := toModelItem(i.M)
		if err != nil {
			return nil, err
		}

		bankItem.MagicalItems = append(bankItem.MagicalItems, *magic_item)
	}

	return &bankItem, nil
}
