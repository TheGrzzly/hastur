package database

import (
	"hastur/model"
	"log"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

const (
	FACTION_TABLE_NAME = "Faction"
)

func (s *Service) CreateFactionTable() error {
	_, err := s.dynamo.CreateTable(&dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("N"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(5),
			WriteCapacityUnits: aws.Int64(5),
		},
		TableName: aws.String(FACTION_TABLE_NAME),
	})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeResourceInUseException:
				log.Println(aerr.Error())
			default:
				return err
			}
		} else {
			return err
		}
	}

	return nil
}

func (s *Service) GetAllFactions() ([]model.Faction, error) {
	params := &dynamodb.ScanInput{
		TableName: aws.String(FACTION_TABLE_NAME),
	}

	result, err := s.dynamo.Scan(params)
	if err != nil {
		return nil, err
	}

	return toModelFactions(result.Items)
}

func (s *Service) GetFactionById(id int) (*model.Faction, error) {
	result, err := s.dynamo.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(FACTION_TABLE_NAME),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				N: aws.String(strconv.Itoa(id)),
			},
		},
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeResourceNotFoundException:
				log.Println(dynamodb.ErrCodeResourceNotFoundException, aerr.Error())
				return nil, nil
			default:
				return nil, err
			}
		} else {
			log.Println("Could not get error from aws error convertion")
			return nil, err
		}
	}
	if result.Item == nil {
		return nil, nil
	}

	return toModelFaction(result.Item)
}

func (s *Service) GetFactionByName(name string) (*model.Faction, error) {
	filt := expression.Name("name").Equal(expression.Value(name))
	proj := expression.NamesList(
		expression.Name("id"),
		expression.Name("name"),
		expression.Name("cap"),
	)
	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()
	if err != nil {
		return nil, err
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      expr.Projection(),
		FilterExpression:          expr.Filter(),
		TableName:                 aws.String(FACTION_TABLE_NAME),
	}

	result, err := s.dynamo.Scan(params)
	if err != nil {
		return nil, err
	}

	if len(result.Items) == 0 {
		return nil, nil
	}

	return toModelFaction(result.Items[0])
}

func (s *Service) PostFaction(faction model.Faction) error {
	attrVal, err := dynamodbattribute.MarshalMap(faction)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      attrVal,
		TableName: aws.String(FACTION_TABLE_NAME),
	}

	_, err = s.dynamo.PutItem(input)
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) PatchFaction(faction model.Faction) error {
	_, err := s.dynamo.UpdateItem(&dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#N": aws.String("name"),
			"#C": aws.String("cap"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":name": {
				S: aws.String(faction.Name),
			},
			":cap": {
				N: aws.String(strconv.Itoa(faction.Cap)),
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				N: aws.String(strconv.Itoa(faction.ID)),
			},
		},
		TableName:        aws.String(FACTION_TABLE_NAME),
		UpdateExpression: aws.String("SET #N = :name, #C = :cap"),
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) DeleteFaction(id int) error {
	_, err := s.dynamo.DeleteItem(&dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				N: aws.String(strconv.Itoa(id)),
			},
		},
		TableName: aws.String(FACTION_TABLE_NAME),
	})

	if err != nil {
		return err
	}

	return nil
}

func toModelFactions(items []map[string]*dynamodb.AttributeValue) ([]model.Faction, error) {
	factions := []model.Faction{}
	for _, i := range items {
		faction, err := toModelFaction(i)
		if err != nil {
			log.Println("Error Transforming to Faction Item")
			return nil, err
		}

		factions = append(factions, *faction)
	}

	return factions, nil
}

func toModelFaction(item map[string]*dynamodb.AttributeValue) (*model.Faction, error) {
	faction := model.Faction{}

	err := dynamodbattribute.UnmarshalMap(item, &faction)
	if err != nil {
		log.Println("Error Transforming to Faction Item")
		return nil, err
	}

	return &faction, nil
}
