package repository

import (
	"encoding/json"
	"hastur/model"
	"hastur/utils"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	cons "hastur/constant"
)

type repoCharacters struct {
	Characters []characterObj `json:"characters"`
}

const timeFormat = "01-02-2006"

type characterObj struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	Race       string `json:"race"`
	Faction    string `json:"faction"`
	Class      string `json:"class"`
	Region     string `json:"region"`
	Level      int    `json:"level"`
	Exp        int    `json:"exp"`
	LastPlayed string `json:"last_played"`
	Sheet      string `json:"sheet"`
}

func (*Repo) GetAllCharacters() ([]model.Character, error) {
	characters, err := getCharactersObj()
	if err != nil {
		return nil, err
	}

	return characters.toModel(), nil
}

func (*Repo) GetCharactersByName(names []string) ([]model.Character, error) {
	characters, err := getCharactersObj()
	if err != nil {
		return nil, err
	}
	characertersResult := make([]model.Character, 0)

	for _, character := range characters.toModel() {
		for _, name := range names {
			if strings.EqualFold(character.Name, name) {
				characertersResult = append(characertersResult, character)
			}
		}
	}

	return characertersResult, nil
}

func (*Repo) GetCharactersByIds(ids []string) ([]model.Character, error) {
	characters, err := getCharactersObj()
	if err != nil {
		return nil, err
	}
	characertersResult := make([]model.Character, 0)

	for _, character := range characters.toModel() {
		for _, id := range ids {
			if id == character.ID {
				characertersResult = append(characertersResult, character)
			}
		}
	}

	return characertersResult, nil
}

func (*Repo) GetCharacterById(id string) (*model.Character, error) {
	characters, err := getCharactersObj()
	if err != nil {
		return nil, err
	}

	for _, character := range characters.toModel() {
		if character.ID == id {
			return &character, nil
		}
	}

	return nil, nil
}

func (*Repo) GetCharacterByName(name string) (*model.Character, error) {
	characters, err := getCharactersObj()
	if err != nil {
		return nil, err
	}

	for _, character := range characters.toModel() {
		if strings.EqualFold(character.Name, name) {
			return &character, nil
		}
	}

	return nil, nil
}

func (*Repo) GetCharactersWithFilter(level int, race, faction, class, region string, date bool) ([]model.Character, error) {
	characters, err := getCharactersObj()
	if err != nil {
		return nil, err
	}

	charactersResult := make([]model.Character, 0)
	for _, character := range characters.toModel() {
		if level != 0 && level != character.Level {
			continue
		}
		if race != "" && !strings.EqualFold(race, character.Race) {
			continue
		}
		if faction != "" && !strings.EqualFold(faction, character.Faction) {
			continue
		}
		if class != "" && !strings.Contains(strings.ToLower(character.Class), strings.ToLower(class)) {
			continue
		}
		if region != "" && !strings.EqualFold(region, character.Region) {
			continue
		}
		charactersResult = append(charactersResult, character)
	}

	if date {
		charactersResult = utils.SortByLastPlayed(charactersResult)
	}

	return charactersResult, nil
}

func (*Repo) PostCharacters(characters []model.Character) error {
	charactersRepo, err := getCharactersObj()
	if err != nil {
		return err
	}

	for _, character := range characters {
		charactersRepo.Characters = append(charactersRepo.Characters, characterObj{
			ID:         character.ID,
			Name:       character.Name,
			Race:       character.Race,
			Faction:    character.Faction,
			Class:      character.Class,
			Region:     character.Region,
			Level:      character.Level,
			Exp:        character.Exp,
			LastPlayed: character.LastPlayed,
			Sheet:      character.Sheet,
		})
	}

	err = writeCharactersObj(charactersRepo)
	if err != nil {
		return err
	}

	return nil
}

func (*Repo) PatchCharacters(characters []model.Character) error {
	charactersRepo, err := getCharactersObj()
	if err != nil {
		return err
	}

	for i, character := range charactersRepo.Characters {
		for _, updated_character := range characters {
			if character.ID == updated_character.ID {
				charactersRepo.Characters[i].Name = updated_character.Name
				charactersRepo.Characters[i].Class = updated_character.Class
				charactersRepo.Characters[i].Faction = updated_character.Faction
				charactersRepo.Characters[i].Level = updated_character.Level
				charactersRepo.Characters[i].Region = updated_character.Region
				charactersRepo.Characters[i].Race = updated_character.Race
				charactersRepo.Characters[i].Exp = updated_character.Exp
				charactersRepo.Characters[i].LastPlayed = updated_character.LastPlayed
				charactersRepo.Characters[i].Sheet = updated_character.Sheet
			}
		}
	}

	err = writeCharactersObj(charactersRepo)
	if err != nil {
		return err
	}

	return nil
}

func (*Repo) DeleteCharacter(id string) error {
	characters, err := getCharactersObj()
	if err != nil {
		return err
	}

	var data []characterObj
	for _, character := range characters.Characters {
		if character.ID != id {
			data = append(data, character)
		}
	}

	err = writeCharactersObj(repoCharacters{Characters: data})
	if err != nil {
		return err
	}

	return nil
}

func (r repoCharacters) toModel() []model.Character {
	characters := make([]model.Character, len(r.Characters))

	for i := range r.Characters {

		t, err := time.Parse(timeFormat, r.Characters[i].LastPlayed)
		if err != nil {
			log.Println(cons.ErrorParsingDateLayout, err)
			t = time.Now()
		}

		characters[i] = model.Character{
			ID:         r.Characters[i].ID,
			Name:       r.Characters[i].Name,
			Race:       r.Characters[i].Race,
			Faction:    r.Characters[i].Faction,
			Class:      r.Characters[i].Class,
			Region:     r.Characters[i].Region,
			Level:      r.Characters[i].Level,
			Exp:        r.Characters[i].Exp,
			LastPlayed: t.Format(timeFormat),
			Sheet:      r.Characters[i].Sheet,
		}
	}

	return characters
}

func getCharactersObj() (repoCharacters, error) {
	charactersFile, err := os.Open(absCharactersFilePath)
	if err != nil {
		return repoCharacters{}, err
	}

	defer charactersFile.Close()

	byteCharacters, err := ioutil.ReadAll(charactersFile)
	if err != nil {
		return repoCharacters{}, err
	}

	var characters repoCharacters
	err = json.Unmarshal(byteCharacters, &characters)
	if err != nil {
		return repoCharacters{}, err
	}

	return characters, nil
}

func writeCharactersObj(characters repoCharacters) error {
	charactersFile, err := os.Open(absCharactersFilePath)
	if err != nil {
		return err
	}

	defer charactersFile.Close()

	file, err := json.MarshalIndent(characters, "", "    ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(absCharactersFilePath, file, 0644)
	if err != nil {
		return err
	}

	return nil
}
