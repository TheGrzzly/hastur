package repository

import (
	"encoding/json"
	"hastur/model"
	"io/ioutil"
	"log"
	"os"
)

type repoUsers struct {
	Users []userObj `json:"users"`
}

type userObj struct {
	User     string `json:"user"`
	Password string `json:"password"`
	IsAdmin  bool   `json:"is_admin"`
	IsDev    bool   `json:"is_dev"`
}

func (*Repo) GetUserByName(username string) (*model.User, error) {
	users, err := getUsersObj()
	if err != nil {
		log.Println(err)

		return nil, err
	}

	for _, user := range users.toModel() {
		if user.User == username {
			return &user, nil
		}
	}

	return nil, nil
}

func (*Repo) InsertUser(user, password string) error {
	users, err := getUsersObj()
	if err != nil {
		log.Println(err)

		return err
	}

	users.Users = append(users.Users, userObj{
		User:     user,
		Password: password,
		IsAdmin:  false,
		IsDev:    false,
	})

	err = writeUsersObj(users)
	if err != nil {
		log.Println(err)

		return err
	}

	return nil
}

func (r repoUsers) toModel() []model.User {
	users := make([]model.User, len(r.Users))

	for i := range r.Users {
		users[i] = model.User{
			User:     r.Users[i].User,
			Password: r.Users[i].Password,
			IsAdmin:  r.Users[i].IsAdmin,
			IsDev:    r.Users[i].IsDev,
		}
	}

	return users
}

func getUsersObj() (repoUsers, error) {
	usersFile, err := os.Open(absUsersFilepath)
	if err != nil {
		return repoUsers{}, err
	}

	defer usersFile.Close()

	byteUsers, err := ioutil.ReadAll(usersFile)
	if err != nil {
		log.Println(err)

		return repoUsers{}, err
	}

	var users repoUsers
	err = json.Unmarshal(byteUsers, &users)
	if err != nil {
		log.Println(err)

		return repoUsers{}, err
	}

	return users, nil
}

func writeUsersObj(users repoUsers) error {
	usersFile, err := os.Open(absUsersFilepath)
	if err != nil {
		return err
	}

	defer usersFile.Close()

	file, err := json.MarshalIndent(users, "", "    ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(absUsersFilepath, file, 0644)
	if err != nil {
		return err
	}

	return nil
}
