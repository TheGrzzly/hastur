package repository

import "path/filepath"

// Repo struct
type Repo struct{}

// New repo function
func New() *Repo {
	return &Repo{}
}

const (
	factionsFilePath    = "../hastur/repository/datasource/factions.json"
	charactersFilePath  = "../hastur/repository/datasource/characters.json"
	inventoriesFilePath = "../hastur/repository/datasource/inventories.json"
	usersFilePath       = "../hastur/repository/datasource/users.json"
)

var (
	absFactionsFilePath    string
	absCharactersFilePath  string
	absInventoriesFilepath string
	absUsersFilepath       string
)

func init() {
	absFactionsFilePath, _ = filepath.Abs(factionsFilePath)
	absCharactersFilePath, _ = filepath.Abs(charactersFilePath)
	absInventoriesFilepath, _ = filepath.Abs(inventoriesFilePath)
	absUsersFilepath, _ = filepath.Abs(usersFilePath)
}
