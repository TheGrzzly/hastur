package repository

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"

	"hastur/model"
)

type repoFactions struct {
	Factions []factionObj `json:"factions"`
}

type factionObj struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Cap  int    `json:"cap"`
}

// GetAllFactions available
func (*Repo) GetAllFactions() ([]model.Faction, error) {
	factions, err := getFactionsObject()
	if err != nil {
		return nil, err
	}

	return factions.toModel(), nil
}

func (*Repo) GetFactionById(id int) (*model.Faction, error) {
	factions, err := getFactionsObject()
	if err != nil {
		return nil, err
	}

	for _, faction := range factions.toModel() {
		if faction.ID == id {
			return &faction, nil
		}
	}

	return nil, nil
}

func (*Repo) GetFactionByName(name string) (*model.Faction, error) {
	factions, err := getFactionsObject()
	if err != nil {
		return nil, err
	}

	for _, faction := range factions.toModel() {
		if strings.EqualFold(faction.Name, name) {
			return &faction, nil
		}
	}

	return nil, nil
}

func (*Repo) PostFaction(faction model.Faction) error {
	factions, err := getFactionsObject()
	if err != nil {
		return err
	}

	id := 1

	if len(factions.Factions) > 0 {
		id = factions.Factions[len(factions.Factions)-1].ID + 1
	}

	factions.Factions = append(factions.Factions, factionObj{
		ID:   id,
		Name: faction.Name,
		Cap:  faction.Cap,
	})

	err = writeFactionsObject(factions)
	if err != nil {
		return err
	}

	return nil
}

func (*Repo) PatchFaction(faction model.Faction) error {
	factions, err := getFactionsObject()
	if err != nil {
		return err
	}

	for i, factionVal := range factions.Factions {
		if factionVal.ID == faction.ID {
			factions.Factions[i].Name = faction.Name
			factions.Factions[i].Cap = faction.Cap
		}
	}

	err = writeFactionsObject(factions)
	if err != nil {
		return err
	}

	return nil
}

func (*Repo) DeleteFaction(id int) error {
	factions, err := getFactionsObject()
	if err != nil {
		return err
	}

	var data []factionObj
	for _, faction := range factions.Factions {
		if faction.ID != id {
			data = append(data, faction)
		}
	}

	err = writeFactionsObject(repoFactions{Factions: data})
	if err != nil {
		return err
	}

	return nil
}

func (r repoFactions) toModel() []model.Faction {
	factions := make([]model.Faction, len(r.Factions))

	for i := range r.Factions {
		factions[i] = model.Faction{
			ID:   r.Factions[i].ID,
			Name: r.Factions[i].Name,
			Cap:  r.Factions[i].Cap,
		}
	}

	return factions
}

func getFactionsObject() (repoFactions, error) {
	factionsFile, err := os.Open(absFactionsFilePath)
	if err != nil {
		return repoFactions{}, err
	}

	defer factionsFile.Close()

	byteFactions, err := ioutil.ReadAll(factionsFile)
	if err != nil {
		return repoFactions{}, err
	}

	var factions repoFactions
	err = json.Unmarshal(byteFactions, &factions)
	if err != nil {
		return repoFactions{}, err
	}

	return factions, nil
}

func writeFactionsObject(factions repoFactions) error {
	factionsFile, err := os.Open(absFactionsFilePath)
	if err != nil {
		return err
	}

	defer factionsFile.Close()

	file, err := json.MarshalIndent(factions, "", "    ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(absFactionsFilePath, file, 0644)
	if err != nil {
		return err
	}

	return nil
}
