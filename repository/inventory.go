package repository

import (
	"encoding/json"
	"hastur/model"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type repoInventories struct {
	Inventories []inventoryObj `json:"inventories"`
}

type inventoryObj struct {
	ID           string    `json:"id"`
	Gold         float64   `json:"gold"`
	Items        []itemObj `json:"items"`
	MagicalItems []itemObj `json:"magical_items"`
}

type itemObj struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Quantity    int    `json:"quantity"`
}

func (*Repo) GetAllInventories() ([]model.Inventory, error) {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return nil, err
	}

	return inventories.toModel(), nil
}

func (*Repo) GetInventoriesByIds(ids []string) ([]model.Inventory, error) {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return nil, err
	}
	inventoriesResult := []model.Inventory{}

	for _, inventory := range inventories.Inventories {
		for _, id := range ids {
			if inventory.ID == id {
				inventoriesResult = append(inventoriesResult, model.Inventory{
					ID:           inventory.ID,
					Gold:         inventory.Gold,
					Items:        inventory.itemsToModel(inventory.Items),
					MagicalItems: inventory.itemsToModel(inventory.MagicalItems),
				})
			}
		}
	}

	return inventoriesResult, nil
}

func (*Repo) GetInventory(id string) (*model.Inventory, error) {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return nil, err
	}

	for _, inventory := range inventories.toModel() {
		if inventory.ID == id {
			return &inventory, nil
		}
	}

	return nil, nil
}

func (*Repo) AddGoldToInventories(ids []string, gold float64) error {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return err
	}

	for i, inventory := range inventories.Inventories {
		for _, id := range ids {
			if inventory.ID == id {
				inventories.Inventories[i].Gold = inventories.Inventories[i].Gold + gold
			}
		}
	}

	err = writeInventoryFile(inventories)
	if err != nil {
		log.Println(err)

		return err
	}

	return nil
}

func (*Repo) InsertInventory(id string, gold float64) error {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return nil
	}

	inventories.Inventories = append(inventories.Inventories, inventoryObj{
		ID:           id,
		Gold:         gold,
		Items:        []itemObj{},
		MagicalItems: []itemObj{},
	})

	err = writeInventoryFile(inventories)
	if err != nil {
		log.Println(err)

		return err
	}

	return nil
}

func (*Repo) GetItems(id string, items []string) ([]string, error) {
	inventores, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return nil, err
	}

	result := []string{}

	for _, inventory := range inventores.Inventories {
		if id == inventory.ID {
			for _, item := range inventory.Items {
				for _, item_search := range items {
					if strings.EqualFold(item.Name, item_search) {
						result = append(result, item.Name)
						break
					}
				}
			}
		}
	}

	return result, nil
}

func (*Repo) GetMagicItems(id string, items []string) ([]string, error) {
	inventores, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return nil, err
	}

	result := []string{}

	for _, inventory := range inventores.Inventories {
		if id == inventory.ID {
			for _, item := range inventory.MagicalItems {
				for _, item_search := range items {
					if strings.EqualFold(item_search, item.Name) {
						result = append(result, item.Name)
						break
					}
				}
			}
		}
	}

	return result, nil
}

func (*Repo) PostItems(id string, items, magical_items []model.Item) error {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return nil
	}

	for i, inventory := range inventories.Inventories {
		if id == inventory.ID {
			flag := false
			for _, item := range items {

				for j, old_item := range inventory.Items {
					if strings.EqualFold(item.Name, old_item.Name) {
						inventories.Inventories[i].Items[j].Quantity = old_item.Quantity + item.Quantity
						flag = true
						break
					}
				}

				if !flag {
					inventories.Inventories[i].Items = append(inventories.Inventories[i].Items, itemObj{
						Name:        item.Name,
						Description: item.Description,
						Quantity:    item.Quantity,
					})
				}
				flag = false
			}

			flag = false
			for _, magic_item := range magical_items {

				for j, old_item := range inventory.MagicalItems {
					if strings.EqualFold(magic_item.Name, old_item.Name) {
						inventories.Inventories[i].MagicalItems[j].Quantity = old_item.Quantity + magic_item.Quantity
						flag = true
						break
					}
				}

				if !flag {
					inventories.Inventories[i].MagicalItems = append(inventories.Inventories[i].MagicalItems, itemObj{
						Name:        magic_item.Name,
						Description: magic_item.Description,
						Quantity:    magic_item.Quantity,
					})
				}
				flag = false
			}
		}
	}

	err = writeInventoryFile(inventories)
	if err != nil {
		log.Println(err)

		return err
	}

	return nil
}

func (*Repo) PatchItems(id string, items, magic_items []model.Item) error {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return err
	}

	for i, inventory := range inventories.Inventories {
		if inventory.ID == id {
			for j, item_old := range inventory.Items {
				for _, item := range items {
					if item_old.Name == item.Name {
						inventories.Inventories[i].Items[j].Description = item.Description
						inventories.Inventories[i].Items[j].Quantity = item.Quantity
						break
					}
				}
			}

			for j, item_old := range inventory.MagicalItems {
				for _, item := range magic_items {
					if item_old.Name == item.Name {
						inventories.Inventories[i].MagicalItems[j].Description = item.Description
						inventories.Inventories[i].MagicalItems[j].Quantity = item.Quantity
						break
					}
				}
			}

			break
		}
	}

	err = writeInventoryFile(inventories)
	if err != nil {
		log.Println(err)

		return err
	}

	return nil
}

func (*Repo) DeleteItems(id string, items []string) error {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return err
	}

	result := []itemObj{}
	for i, inventory := range inventories.Inventories {
		if inventory.ID == id {
			for _, item_old := range inventory.Items {
				needsToBeDeleted := false
				for _, item := range items {
					if item == item_old.Name {
						needsToBeDeleted = true
						break
					}
				}

				if !needsToBeDeleted {
					result = append(result, itemObj{
						Name:        item_old.Name,
						Description: item_old.Description,
						Quantity:    item_old.Quantity,
					})
				}
			}

			inventories.Inventories[i].Items = result
			break
		}
	}

	err = writeInventoryFile(inventories)
	if err != nil {
		log.Println(err)

		return err
	}

	return nil
}

func (*Repo) DeleteMagicItems(id string, items []string) error {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return err
	}

	result := []itemObj{}
	for i, inventory := range inventories.Inventories {
		if inventory.ID == id {
			for _, item_old := range inventory.MagicalItems {
				needsToBeDeleted := false
				for _, item := range items {
					if item == item_old.Name {
						needsToBeDeleted = true
						break
					}
				}

				if !needsToBeDeleted {
					result = append(result, itemObj{
						Name:        item_old.Name,
						Description: item_old.Description,
						Quantity:    item_old.Quantity,
					})
				}
			}

			inventories.Inventories[i].MagicalItems = result
			break
		}
	}

	err = writeInventoryFile(inventories)
	if err != nil {
		log.Println(err)

		return err
	}

	return nil
}

func (*Repo) DeleteInventory(id string) error {
	inventories, err := getInventoryObj()
	if err != nil {
		log.Println(err)

		return err
	}

	var inventoryResult []inventoryObj
	for _, inventory := range inventories.Inventories {
		if inventory.ID != id {
			inventoryResult = append(inventoryResult, inventory)
		}
	}

	err = writeInventoryFile(repoInventories{
		Inventories: inventoryResult,
	})
	if err != nil {
		log.Println(err)

		return err
	}

	return nil
}

func (r repoInventories) toModel() []model.Inventory {
	inventories := make([]model.Inventory, len(r.Inventories))

	for i := range r.Inventories {
		inventories[i] = model.Inventory{
			ID:           r.Inventories[i].ID,
			Gold:         r.Inventories[i].Gold,
			Items:        r.Inventories[i].itemsToModel(r.Inventories[i].Items),
			MagicalItems: r.Inventories[i].itemsToModel(r.Inventories[i].MagicalItems),
		}
	}

	return inventories
}

func (inventoryObj) itemsToModel(item_slice []itemObj) []model.Item {
	items := make([]model.Item, len(item_slice))

	for i := range item_slice {
		items[i] = model.Item{
			Name:        item_slice[i].Name,
			Description: item_slice[i].Description,
			Quantity:    item_slice[i].Quantity,
		}
	}

	return items
}

func getInventoryObj() (repoInventories, error) {
	inventoryFile, err := os.Open(absInventoriesFilepath)
	if err != nil {
		return repoInventories{}, err
	}

	defer inventoryFile.Close()

	byteInventories, err := ioutil.ReadAll(inventoryFile)
	if err != nil {
		return repoInventories{}, err
	}

	var inventories repoInventories
	err = json.Unmarshal(byteInventories, &inventories)
	if err != nil {
		return repoInventories{}, err
	}

	return inventories, nil
}

func writeInventoryFile(inventories repoInventories) error {
	inventoryFile, err := os.Open(absInventoriesFilepath)
	if err != nil {
		return err
	}

	defer inventoryFile.Close()

	file, err := json.MarshalIndent(inventories, "", "    ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(absInventoriesFilepath, file, 0644)
	if err != nil {
		return err
	}

	return nil
}
