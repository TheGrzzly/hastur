package middleware

import (
	"net/http"

	"github.com/unrolled/render"

	cons "hastur/constant"
)

type JWT interface {
	ValidateAdmin(token string) bool
	ValidateDevAccess(token string) bool
}

type Middleware struct {
	jwt    JWT
	render *render.Render
}

func New(j JWT, r *render.Render) *Middleware {
	return &Middleware{
		jwt:    j,
		render: r,
	}
}

func (m *Middleware) ValidateJWTAdmin(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		if !m.jwt.ValidateAdmin(req.Header.Get("Token")) {
			m.render.Text(w, http.StatusUnauthorized, cons.TokenIsntValid)
			return
		}

		handler(w, req)
	}
}

func (m *Middleware) ValidateJWTDev(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		if !m.jwt.ValidateDevAccess(req.Header.Get("Token")) {
			m.render.Text(w, http.StatusUnauthorized, cons.TokenIsntValid)
			return
		}

		handler(w, req)
	}
}
