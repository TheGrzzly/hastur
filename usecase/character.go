package usecase

import (
	"hastur/model"
	"hastur/utils"
	"log"
	"net/http"
	"strings"

	cons "hastur/constant"
)

// Database Service Interface
type CharacterDatabase interface {
	GetAllCharacters() ([]model.Character, error)
	GetCharacterById(id string) (*model.Character, error)
	GetCharacterByName(name string) (*model.Character, error)
	GetCharactersWithFilter(level int, race, faction, class, region string, date bool) ([]model.Character, error)
	PostCharacter(model.Character) error
	PatchCharacter(model.Character) error
	DeleteCharacter(id string) error
	GetDowntimeDaysById(id string) (*model.DowntimeDaysResponse, error)
	UpdateDowntimeDays(id string, dtds int) error
	GetExperienceById(id string) (*model.ExperienceResponse, error)
	UpdateExperience(id string, exp int) error
}

const (
	DowntimeDaysDefault = 7
)

// character faction struct
type Character struct {
	db CharacterDatabase
}

func NewCharacter(db CharacterDatabase) *Character {
	return &Character{
		db: db,
	}
}

func (u *Character) GetCharacters(params *model.CharacterQueryParams) model.Response {
	if params.ID == "" && params.Faction == "" && params.Level == 0 && params.Name == "" && params.Race == "" && params.Class == "" && !params.LastPlayed {
		characters, err := u.db.GetAllCharacters()

		if err != nil {
			return model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.ErrorReadingCharacters,
			}
		}

		return model.Response{
			Code:    http.StatusOK,
			Message: characters,
		}
	}

	if params.ID != "" {
		character, err := u.db.GetCharacterById(params.ID)
		if err != nil {
			return model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.ErrorReadingCharacters,
			}
		}

		if character == nil {
			return model.Response{
				Code:    http.StatusNotFound,
				Message: cons.CharacterNotFound,
			}
		}

		return model.Response{
			Code:    http.StatusOK,
			Message: character,
		}
	}

	if params.Name != "" {
		character, err := u.db.GetCharacterByName(params.Name)
		if err != nil {
			return model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.ErrorReadingCharacters,
			}
		}

		if character == nil {
			return model.Response{
				Code:    http.StatusNotFound,
				Message: cons.CharacterNotFound,
			}
		}

		return model.Response{
			Code:    http.StatusOK,
			Message: character,
		}
	}

	characters, err := u.db.GetCharactersWithFilter(params.Level, params.Race, params.Faction, params.Class, params.Region, params.LastPlayed)
	if err != nil {
		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.ErrorReadingCharacters,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: characters,
	}
}

func (u *Character) PostCharacter(params *model.CharacterParams) model.Response {
	if params.ReqCharacter.ID == "" || params.ReqCharacter.Name == "" || params.ReqCharacter.Class == "" || params.ReqCharacter.Level == 0 || params.ReqCharacter.Exp == 0 || params.ReqCharacter.Race == "" ||
		!utils.ValidateDates(params.ReqCharacter.LastPlayed, params.ReqCharacter.LastExpEarnedDate, params.ReqCharacter.LastEventPartDate, params.ReqCharacter.LastEventExpDate, params.ReqCharacter.LastRumorPartDate) {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.CharacterBadRequest,
		}
	}

	character, err := u.db.GetCharacterById(params.ReqCharacter.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if character != nil {
		log.Println(character)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.CharacterAlreadyExists,
		}
	}

	character, err = u.db.GetCharacterByName(params.ReqCharacter.Name)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if character != nil {
		log.Println(character)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.CharacterAlreadyExists,
		}
	}

	err = u.db.PostCharacter(*params.ReqCharacter)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	character, err = u.db.GetCharacterById(params.ReqCharacter.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if character == nil {
		log.Println(character)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: character,
	}
}

func (u *Character) PatchCharacter(params *model.CharacterParams) model.Response {
	if params.ReqCharacter.ID == "" || params.ReqCharacter.Name == "" || params.ReqCharacter.Class == "" || params.ReqCharacter.Level == 0 || params.ReqCharacter.Exp == 0 || params.ReqCharacter.Race == "" ||
		!utils.ValidateDates(params.ReqCharacter.LastPlayed, params.ReqCharacter.LastExpEarnedDate, params.ReqCharacter.LastEventPartDate, params.ReqCharacter.LastEventExpDate, params.ReqCharacter.LastRumorPartDate) {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.CharacterBadRequest,
		}
	}

	character, err := u.db.GetCharacterById(params.ReqCharacter.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if character == nil {
		log.Println(character)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.CharacterNotFoundBadRequest,
		}
	}

	character, err = u.db.GetCharacterByName(params.ReqCharacter.Name)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if character != nil {
		if !strings.EqualFold(character.ID, params.ReqCharacter.ID) {
			log.Println(character)

			return model.Response{
				Code:    http.StatusBadRequest,
				Message: cons.CharacterWithNameExists,
			}
		}
	}

	err = u.db.PatchCharacter(*params.ReqCharacter)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	character, err = u.db.GetCharacterById(params.ReqCharacter.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if character == nil {
		log.Println(character)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: character,
	}
}

func (u *Character) DeleteCharacter(params *model.CharacterQueryParams) model.Response {
	if params.ID == "" {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.InvalidCharacter,
		}
	}

	character, err := u.db.GetCharacterById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if character == nil {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.CharacterDoesntExist,
		}
	}

	err = u.db.DeleteCharacter(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	character, err = u.db.GetCharacterById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if character != nil {
		log.Println(character)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: cons.CharacterDeletedSuccesfully,
	}
}

func (u *Character) GetDowntimeDays(params *model.CharacterQueryParams) model.Response {
	if params.ID == "" {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	dtds, err := u.db.GetDowntimeDaysById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if dtds == nil {
		return model.Response{
			Code:    http.StatusNotFound,
			Message: cons.CharacterNotFound,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: dtds,
	}
}

func (u *Character) PatchDowntimeDays(params *model.CharacterQueryParams) model.Response {
	if params.ID == "" || params.DTDs == 0 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	dtds, err := u.db.GetDowntimeDaysById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if dtds == nil {
		return model.Response{
			Code:    http.StatusNotFound,
			Message: cons.CharacterNotFound,
		}
	}

	if dtds.DTDs < params.DTDs {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.NotEnoughDowntimeDays,
		}
	}

	err = u.db.UpdateDowntimeDays(params.ID, dtds.DTDs-params.DTDs)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	dtds, err = u.db.GetDowntimeDaysById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if dtds == nil {
		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: dtds,
	}
}

func (u *Character) PostDowntimeDays(params *model.CharacterQueryParams) model.Response {
	dtds := DowntimeDaysDefault
	if params.DTDs != 0 {
		dtds = params.DTDs
	}

	log.Println(dtds)
	chars, err := u.db.GetAllCharacters()
	if err != nil {
		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.ErrorReadingCharacters,
		}
	}

	// In the future might be a good idea to add concurrency to all of this requests
	for _, char := range chars {
		err = u.db.UpdateDowntimeDays(char.ID, dtds)
		if err != nil {
			log.Println(err)

			return model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: cons.DownTimeDaysReset,
	}
}

func (u *Character) GetExperience(params *model.CharacterQueryParams) model.Response {
	if params.ID == "" {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	exp, err := u.db.GetExperienceById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if exp == nil {
		return model.Response{
			Code:    http.StatusNotFound,
			Message: cons.CharacterNotFound,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: exp,
	}
}

func (u *Character) PatchExperience(params *model.CharacterQueryParams) model.Response {
	if params.ID == "" || params.Exp == 0 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	exp, err := u.db.GetExperienceById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if exp == nil {
		return model.Response{
			Code:    http.StatusNotFound,
			Message: cons.CharacterNotFound,
		}
	}

	err = u.db.UpdateExperience(params.ID, exp.Exp+params.Exp)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	exp, err = u.db.GetExperienceById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if exp == nil {
		return model.Response{
			Code:    http.StatusNotFound,
			Message: cons.CharacterNotFound,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: exp,
	}
}
