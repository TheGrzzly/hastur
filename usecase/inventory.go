package usecase

import (
	"hastur/model"
	"hastur/utils"
	"log"
	"net/http"
	"strings"

	cons "hastur/constant"
)

// InventoryDatabase interface for aws connection
type InventoryDatabase interface {
	GetAllInventories() ([]model.Inventory, error)
	GetInventoriesByIds(ids []string) ([]model.Inventory, error)
	GetInventory(id string) (*model.Inventory, error)
	AddGoldToInventory(id string, gold float64) error
	InsertInventory(id string, gold float64) error
	DeleteInventory(id string) error
	GetItems(id string, items map[string]bool) ([]string, error)
	GetMagicItems(id string, items map[string]bool) ([]string, error)
	PostItems(id string, items, magical_items []model.Item, inventory model.Inventory) error
	PatchItems(id string, items, magical_items []model.Item, inventory model.Inventory) error
	DeleteItems(id string, items map[string]bool, inventory []model.Item) error
	DeleteMagicItems(id string, items map[string]bool, inventory []model.Item) error
	AddGoldToBank(id string, bank model.Bank) error
	UpdateBankItems(id string, items []model.Item, bank model.Bank) error
	UpdateBankMagicItems(id string, items []model.Item, bank model.Bank) error
}

type Inventory struct {
	db InventoryDatabase
}

func NewInventory(db InventoryDatabase) *Inventory {
	return &Inventory{
		db: db,
	}
}

func (u Inventory) GetInventories(params *model.InventoryQueryParams) model.Response {
	if len(params.Ids) == 0 {
		inventories, err := u.db.GetAllInventories()
		if err != nil {
			log.Println(err)

			return model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}

		return model.Response{
			Code:    http.StatusOK,
			Message: inventories,
		}
	}

	inventories, err := u.db.GetInventoriesByIds(params.Ids)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: inventories,
	}
}

func (u Inventory) GetInventory(params *model.InventoryQueryParams) model.Response {
	if params.ID == "" {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		return model.Response{
			Code:    http.StatusNotFound,
			Message: cons.InventoryNotFound,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: inventory,
	}
}

func (u Inventory) PostInventory(params *model.InventoryQueryParams) model.Response {
	if params.ID == "" {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory != nil {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.InventoryWithIdAlreadyExists,
		}
	}

	err = u.db.InsertInventory(params.ID, params.Gold)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	inventory, err = u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: inventory,
	}
}

func (u Inventory) DeleteInventory(params *model.InventoryQueryParams) model.Response {
	if params.ID == "" {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.InventoryNotFound,
		}
	}

	err = u.db.DeleteInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	inventory, err = u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory != nil {
		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: cons.InventoryDeletedSuccess,
	}
}

func (u Inventory) PatchGold(params *model.InventoryQueryParams) model.Response {
	if len(params.Ids) == 0 || params.Gold == 0 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventories, err := u.db.GetInventoriesByIds(params.Ids)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if len(inventories) != len(params.Ids) {
		log.Println(inventories)
		log.Println(len(inventories))
		log.Println(len(params.Ids))

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	for _, i := range inventories {
		err = u.db.AddGoldToInventory(params.ID, params.Gold+i.Gold)

		if err != nil {
			log.Println(err)

			return model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}
	}

	inventories, err = u.db.GetInventoriesByIds(params.Ids)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if len(inventories) != len(params.Ids) {
		log.Println(inventories)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: inventories,
	}
}

func (u Inventory) PostItems(params *model.InventoryParams) model.Response {
	if params.ID == "" || (len(params.MagicalItems) == 0 && len(params.Items) == 0) {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	err = u.db.PostItems(params.ID, params.Items, params.MagicalItems, *inventory)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	inventory, err = u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: inventory,
	}
}

func (u Inventory) PatchItems(params *model.InventoryParams) model.Response {
	if params.ID == "" || (len(params.Items) == 0 && len(params.MagicalItems) == 0) {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	err = u.db.PatchItems(params.ID, params.Items, params.MagicalItems, *inventory)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	inventory, err = u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: inventory,
	}
}

func (u Inventory) DeleteItems(params *model.InventoryQueryParams) model.Response {
	if params.ID == "" || len(params.Names) == 0 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println(inventory)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}
	names := map[string]bool{}
	for _, name := range params.Names {
		names[strings.ToLower(name)] = true
	}

	items, err := u.db.GetItems(params.ID, names)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if len(params.Names) != len(items) {
		log.Println(items)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	err = u.db.DeleteItems(params.ID, names, inventory.Items)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	inventory, err = u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: inventory,
	}
}

func (u Inventory) DeleteMagicItems(params *model.InventoryQueryParams) model.Response {
	if params.ID == "" || len(params.Names) == 0 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println(inventory)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}
	names := map[string]bool{}
	for _, name := range params.Names {
		names[strings.ToLower(name)] = true
	}

	items, err := u.db.GetMagicItems(params.ID, names)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if len(params.Names) != len(items) {
		log.Println(items)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	err = u.db.DeleteMagicItems(params.ID, names, inventory.MagicalItems)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	inventory, err = u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: inventory,
	}
}

func (u Inventory) PatchBankGold(params *model.InventoryQueryParams) model.Response {
	if params.ID == "" || params.Gold == 0 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println("Character does not exist.")

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	inventoryGold := 0.0
	bankGold := 0.0
	if params.ToBank {
		if inventory.Gold < params.Gold {
			return model.Response{
				Code:    http.StatusBadRequest,
				Message: cons.BadRequestSent,
			}
		}
		inventoryGold = inventory.Gold - params.Gold
		bankGold = inventory.Bank.Gold + params.Gold
	} else {
		if inventory.Bank.Gold < params.Gold {
			return model.Response{
				Code:    http.StatusBadRequest,
				Message: cons.BadRequestSent,
			}
		}
		inventoryGold = inventory.Gold + params.Gold
		bankGold = inventory.Bank.Gold - params.Gold
	}
	inventory.Bank.Gold = bankGold

	err = u.db.AddGoldToInventory(params.ID, inventoryGold)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	err = u.db.AddGoldToBank(params.ID, inventory.Bank)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	inventory, err = u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println("Character does not exist.")

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: inventory,
	}
}

func (u Inventory) PatchBankItems(params *model.BankingParams) model.Response {
	if params.ID == "" || len(params.Items) < 1 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	for _, item := range params.Items {
		if item.Name == "" || item.Quantity < 1 {
			return model.Response{
				Code:    http.StatusBadRequest,
				Message: cons.BadRequestSent,
			}
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println("Character does not exist.")

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	itemMap := map[string]model.Item{}
	if params.ToBank {
		itemMap = utils.ItemsArrayIntoMap(inventory.Items)
	} else {
		itemMap = utils.ItemsArrayIntoMap(inventory.Bank.Items)
	}

	if !utils.ValidateItemsInMap(params.Items, itemMap) {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: http.StatusBadRequest,
		}
	}

	//Logic to move items from one inv to bank and visceversa
	invMap := utils.ItemsArrayIntoMap(inventory.Items)
	bankmap := utils.ItemsArrayIntoMap(inventory.Bank.Items)
	if params.ToBank {
		for _, i := range params.Items {
			itemVal := model.Item{}
			if val, ok := invMap[strings.ToLower(i.Name)]; ok {
				itemVal = val
				if val.Quantity > i.Quantity {
					val.Quantity -= i.Quantity
					invMap[strings.ToLower(i.Name)] = val
				} else if val.Quantity == i.Quantity {
					delete(invMap, strings.ToLower(i.Name))
				}
			}
			if val, ok := bankmap[strings.ToLower(i.Name)]; ok {
				val.Quantity += i.Quantity
				bankmap[strings.ToLower(i.Name)] = val
			} else {
				bankmap[strings.ToLower(i.Name)] = itemVal
			}
		}
	} else {
		for _, i := range params.Items {
			itemVal := model.Item{}
			if val, ok := bankmap[strings.ToLower(i.Name)]; ok {
				itemVal = val
				if val.Quantity > i.Quantity {
					val.Quantity -= i.Quantity
					bankmap[strings.ToLower(i.Name)] = val
				} else if val.Quantity == i.Quantity {
					delete(bankmap, strings.ToLower(i.Name))
				}
			}
			if val, ok := bankmap[strings.ToLower(i.Name)]; ok {
				val.Quantity += i.Quantity
				invMap[strings.ToLower(i.Name)] = val
			} else {
				invMap[strings.ToLower(i.Name)] = itemVal
			}
		}
	}
	newInvItems := []model.Item{}
	for _, i := range invMap {
		newInvItems = append(newInvItems, i)
	}

	newBankItems := []model.Item{}
	for _, i := range bankmap {
		newBankItems = append(newBankItems, i)
	}
	newBank := model.Bank{
		Gold:         inventory.Bank.Gold,
		Items:        newBankItems,
		MagicalItems: inventory.Bank.MagicalItems,
	}
	//TODO Logic to update items and bank in database
	err = u.db.UpdateBankItems(params.ID, newInvItems, newBank)
	if err != nil {
		log.Println("Error updating bank items")

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: "lol bad",
	}
}

func (u Inventory) PatchBankMagicItems(params *model.BankingParams) model.Response {
	if params.ID == "" || len(params.Items) < 1 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	for _, item := range params.Items {
		if item.Name == "" || item.Quantity < 1 {
			return model.Response{
				Code:    http.StatusBadRequest,
				Message: cons.BadRequestSent,
			}
		}
	}

	inventory, err := u.db.GetInventory(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if inventory == nil {
		log.Println("Character does not exist.")

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.BadRequestSent,
		}
	}

	itemMap := map[string]model.Item{}
	if params.ToBank {
		itemMap = utils.ItemsArrayIntoMap(inventory.MagicalItems)
	} else {
		itemMap = utils.ItemsArrayIntoMap(inventory.Bank.MagicalItems)
	}

	if !utils.ValidateItemsInMap(params.Items, itemMap) {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: http.StatusBadRequest,
		}
	}

	//Logic to move items from one inv to bank and visceversa
	invMap := utils.ItemsArrayIntoMap(inventory.MagicalItems)
	bankmap := utils.ItemsArrayIntoMap(inventory.Bank.MagicalItems)
	if params.ToBank {
		for _, i := range params.Items {
			itemVal := model.Item{}
			if val, ok := invMap[strings.ToLower(i.Name)]; ok {
				itemVal = val
				if val.Quantity > i.Quantity {
					val.Quantity -= i.Quantity
					invMap[strings.ToLower(i.Name)] = val
				} else if val.Quantity == i.Quantity {
					delete(invMap, strings.ToLower(i.Name))
				}
			}
			if val, ok := bankmap[strings.ToLower(i.Name)]; ok {
				val.Quantity += i.Quantity
				bankmap[strings.ToLower(i.Name)] = val
			} else {
				bankmap[strings.ToLower(i.Name)] = itemVal
			}
		}
	} else {
		for _, i := range params.Items {
			itemVal := model.Item{}
			if val, ok := bankmap[strings.ToLower(i.Name)]; ok {
				itemVal = val
				if val.Quantity > i.Quantity {
					val.Quantity -= i.Quantity
					bankmap[strings.ToLower(i.Name)] = val
				} else if val.Quantity == i.Quantity {
					delete(bankmap, strings.ToLower(i.Name))
				}
			}
			if val, ok := bankmap[strings.ToLower(i.Name)]; ok {
				val.Quantity += i.Quantity
				invMap[strings.ToLower(i.Name)] = val
			} else {
				invMap[strings.ToLower(i.Name)] = itemVal
			}
		}
	}
	newInvItems := []model.Item{}
	for _, i := range invMap {
		newInvItems = append(newInvItems, i)
	}

	newBankItems := []model.Item{}
	for _, i := range bankmap {
		newBankItems = append(newBankItems, i)
	}
	newBank := model.Bank{
		Gold:         inventory.Bank.Gold,
		Items:        inventory.Bank.Items,
		MagicalItems: newBankItems,
	}
	//TODO Logic to update items and bank in database
	err = u.db.UpdateBankMagicItems(params.ID, newInvItems, newBank)
	if err != nil {
		log.Println("Error updating bank items")

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: "lol bad",
	}
}
