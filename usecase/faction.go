package usecase

import (
	"hastur/model"
	"log"
	"net/http"

	cons "hastur/constant"
)

// FactionDatabase interface
type FactionDatabase interface {
	GetAllFactions() ([]model.Faction, error)
	GetFactionById(int) (*model.Faction, error)
	GetFactionByName(string) (*model.Faction, error)
	PostFaction(model.Faction) error
	PatchFaction(model.Faction) error
	DeleteFaction(int) error
}

// Faction usecase struct
type Faction struct {
	db FactionDatabase
}

// NewFaction returns new faction usecase
func NewFaction(db FactionDatabase) *Faction {
	return &Faction{
		db: db,
	}
}

func (u *Faction) GetAllFactions() model.Response {
	factions, err := u.db.GetAllFactions()
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.ErrorReadingFactions,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: factions,
	}
}

func (u *Faction) GetFaction(params *model.FactionQueryParams) model.Response {
	if params.ID == 0 && params.Name == "" {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.EnterParameterForSearch,
		}
	}
	var faction *model.Faction
	var err error
	if params.ID != 0 {
		faction, err = u.db.GetFactionById(params.ID)
	} else {
		faction, err = u.db.GetFactionByName(params.Name)
	}
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.ErrorReadingFactions,
		}
	}

	if faction == nil {
		return model.Response{
			Code:    http.StatusNotFound,
			Message: cons.FactionNotFound,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: faction,
	}
}

func (u *Faction) PostFaction(params *model.FactionParams) model.Response {
	if params.ReqFaction.ID == 0 || params.ReqFaction.Name == "" || params.ReqFaction.Cap == 0 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.InvalidFaction,
		}
	}
	faction, err := u.db.GetFactionById(params.ReqFaction.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if faction != nil {
		log.Println(faction)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.FactionAlreadyExists,
		}
	}

	faction, err = u.db.GetFactionByName(params.ReqFaction.Name)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if faction != nil {
		log.Println(faction)

		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.FactionAlreadyExists,
		}
	}

	err = u.db.PostFaction(*params.ReqFaction)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	faction, err = u.db.GetFactionById(params.ReqFaction.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if faction == nil {
		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.ErrorPostingFactions,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: faction,
	}
}

func (u *Faction) PatchFaction(params *model.FactionParams) model.Response {
	if params.ReqFaction.Name == "" || params.ReqFaction.Cap == 0 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.InvalidFaction,
		}
	}
	faction, err := u.db.GetFactionById(params.ReqFaction.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if faction == nil {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.FactionDoesntExist,
		}
	}

	faction, err = u.db.GetFactionByName(params.ReqFaction.Name)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if faction != nil {
		if faction.ID != params.ReqFaction.ID {
			return model.Response{
				Code:    http.StatusBadRequest,
				Message: cons.FactionWithNameExists,
			}
		}
	}

	err = u.db.PatchFaction(*params.ReqFaction)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	faction, err = u.db.GetFactionById(params.ReqFaction.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if faction == nil {
		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.ErrorPostingFactions,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: faction,
	}
}

func (u *Faction) DeleteFaction(params *model.FactionQueryParams) model.Response {
	if params.ID == 0 {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.InvalidFaction,
		}
	}

	faction, err := u.db.GetFactionById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if faction == nil {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.FactionDoesntExist,
		}
	}

	err = u.db.DeleteFaction(params.ID)
	if err != nil {
		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	faction, err = u.db.GetFactionById(params.ID)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if faction != nil {
		log.Println(faction)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: cons.FactionDeletedSuccesfully,
	}
}
