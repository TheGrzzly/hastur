package usecase

import (
	"hastur/model"
	"log"
	"net/http"

	cons "hastur/constant"
)

type Crypter interface {
	EncryptPassword(passwd string) string
	DoesPasswordsMatch(reqPasswd, dbPasswd string) bool
}

type JWT interface {
	Generate(user string, isAdmin, devAccess bool) (string, error)
}

type RepoUser interface {
	GetUserByName(username string) (*model.User, error)
	InsertUser(user, password string) error
}

type User struct {
	repo    RepoUser
	crypter Crypter
	jwt     JWT
}

func NewUser(r RepoUser, c Crypter, j JWT) *User {
	return &User{
		repo:    r,
		crypter: c,
		jwt:     j,
	}
}

func (u *User) Signup(params *model.UserParams) model.Response {
	user, err := u.repo.GetUserByName(params.User)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if user != nil {
		return model.Response{
			Code:    http.StatusBadRequest,
			Message: cons.UserAlreadyExists,
		}
	}

	err = u.repo.InsertUser(params.User, u.crypter.EncryptPassword(params.Password))
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code:    http.StatusOK,
		Message: cons.UserSignupSuccess,
	}
}

func (u *User) Login(params *model.UserParams) model.Response {
	user, err := u.repo.GetUserByName(params.User)
	if err != nil {
		log.Println(err)

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}
	if user == nil || !u.crypter.DoesPasswordsMatch(params.Password, user.Password) {
		return model.Response{
			Code:    http.StatusUnauthorized,
			Message: cons.PasswordNotMatch,
		}
	}

	token, err := u.jwt.Generate(user.User, user.IsAdmin, user.IsDev)
	if err != nil {
		log.Println(cons.ErrorGeneratingToken, err.Error())

		return model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return model.Response{
		Code: http.StatusOK,
		Message: model.LoginResponse{
			Message: cons.LogingSuccess,
			Token:   token,
		},
	}
}
