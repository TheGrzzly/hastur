package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"hastur/model"

	"github.com/unrolled/render"

	cons "hastur/constant"
)

type FactionUseCase interface {
	GetAllFactions() model.Response
	GetFaction(*model.FactionQueryParams) model.Response
	PostFaction(*model.FactionParams) model.Response
	PatchFaction(*model.FactionParams) model.Response
	DeleteFaction(*model.FactionQueryParams) model.Response
}

// Faction controller struct
type Faction struct {
	render  *render.Render
	usecase FactionUseCase
}

// New faction controller
func NewFaction(r *render.Render, u FactionUseCase) *Faction {
	return &Faction{
		render:  r,
		usecase: u,
	}
}

func (c *Faction) GetFactions(w http.ResponseWriter, r *http.Request) {
	resp := c.usecase.GetAllFactions()
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c *Faction) GetFaction(w http.ResponseWriter, r *http.Request) {
	queryParams, err := getQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.Text(w, err.Code, cons.ErrorParsingQueryParameters)

		return
	}
	resp := c.usecase.GetFaction(queryParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c *Faction) PostFaction(w http.ResponseWriter, r *http.Request) {
	factionParams, err := getFactionParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PostFaction(factionParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c *Faction) PatchFaction(w http.ResponseWriter, r *http.Request) {
	factionParams, err := getFactionParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PatchFaction(factionParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c *Faction) DeleteFaction(w http.ResponseWriter, r *http.Request) {
	queryParams, err := getQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.Text(w, err.Code, cons.ErrorParsingQueryParameters)

		return
	}
	resp := c.usecase.DeleteFaction(queryParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func getQueryParams(req *http.Request) (*model.FactionQueryParams, *model.Response) {
	keys := req.URL.Query()
	idParam, ok1 := keys["id"]
	nameParam, ok2 := keys["name"]
	if !ok1 && !ok2 {
		return nil, &model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.ErrorGettingQueryParams,
		}
	}

	queryParams := &model.FactionQueryParams{
		ID:   0,
		Name: "",
	}

	if len(idParam) != 0 {
		id, err := strconv.Atoi(idParam[0])
		if err != nil {
			log.Println(cons.ErrorParsingQueryParameters, err.Error())

			return nil, &model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}
		queryParams.ID = id
	}

	if len(nameParam) != 0 {
		queryParams.Name = nameParam[0]
	}

	return queryParams, nil
}

func getFactionParams(req *http.Request) (*model.FactionParams, *model.Response) {

	var reqFaction model.Faction

	if req.Method == "POST" || req.Method == "PATCH" {
		err := json.NewDecoder(req.Body).Decode(&reqFaction)
		if err != nil {
			log.Println(cons.ErrorParsingBodyParamters, err.Error())

			return nil, &model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}
	}

	return &model.FactionParams{
		ReqFaction: &reqFaction,
	}, nil
}
