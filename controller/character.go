package controller

import (
	"encoding/json"
	"hastur/model"
	"log"
	"net/http"
	"strconv"

	cons "hastur/constant"

	"github.com/unrolled/render"
)

type CharacterUseCase interface {
	GetCharacters(*model.CharacterQueryParams) model.Response
	PostCharacter(*model.CharacterParams) model.Response
	PatchCharacter(*model.CharacterParams) model.Response
	DeleteCharacter(*model.CharacterQueryParams) model.Response
	GetDowntimeDays(*model.CharacterQueryParams) model.Response
	PatchDowntimeDays(*model.CharacterQueryParams) model.Response
	PostDowntimeDays(*model.CharacterQueryParams) model.Response
	GetExperience(*model.CharacterQueryParams) model.Response
	PatchExperience(*model.CharacterQueryParams) model.Response
}

type Character struct {
	render  *render.Render
	usecase CharacterUseCase
}

func NewCharacter(r *render.Render, u CharacterUseCase) *Character {
	return &Character{
		render:  r,
		usecase: u,
	}
}

func (c Character) GetCharacters(w http.ResponseWriter, r *http.Request) {
	queryParams, err := getCharacterQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.Text(w, err.Code, cons.ErrorParsingQueryParameters)

		return
	}
	resp := c.usecase.GetCharacters(queryParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Character) PostCharacter(w http.ResponseWriter, r *http.Request) {
	characterParams, err := getCharactersParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PostCharacter(characterParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Character) PatchCharacter(w http.ResponseWriter, r *http.Request) {
	characterParams, err := getCharactersParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PatchCharacter(characterParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Character) DeleteCharacter(w http.ResponseWriter, r *http.Request) {
	characterParams, err := getCharacterQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.DeleteCharacter(characterParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Character) GetDowntimeDays(w http.ResponseWriter, r *http.Request) {
	characterParams, err := getCharacterQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.GetDowntimeDays(characterParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Character) PatchDowntimeDays(w http.ResponseWriter, r *http.Request) {
	characterParams, err := getCharacterQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PatchDowntimeDays(characterParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Character) PostDowntimeDays(w http.ResponseWriter, r *http.Request) {
	characterParams, err := getCharacterQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PostDowntimeDays(characterParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Character) GetExperience(w http.ResponseWriter, r *http.Request) {
	characterParams, err := getCharacterQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.GetExperience(characterParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Character) PatchExperience(w http.ResponseWriter, r *http.Request) {
	characterParams, err := getCharacterQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PatchExperience(characterParams)
	c.render.JSON(w, resp.Code, resp.Message)
}

func getCharacterQueryParams(req *http.Request) (*model.CharacterQueryParams, *model.Response) {
	keys := req.URL.Query()
	keyStrings := []string{"id", "name", "faction", "level", "race", "region", "class", "last_played", "exp", "dtds"}
	keyvals := make(map[string]string)
	for _, keyString := range keyStrings {
		value, exists := keys[keyString]
		if exists && len(value) > 0 {
			keyvals[keyString] = value[0]
		}
	}

	queryParams := &model.CharacterQueryParams{
		ID:         "",
		Name:       "",
		Faction:    "",
		Level:      0,
		Race:       "",
		Region:     "",
		Class:      "",
		Exp:        0,
		DTDs:       0,
		LastPlayed: false,
	}

	if keyvals["level"] != "" {
		level, err := strconv.Atoi(keyvals["level"])
		if err != nil {
			log.Println(cons.ErrorParsingQueryParameters, err.Error())

			return nil, &model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}

		queryParams.Level = level
	}

	if keyvals["exp"] != "" {
		exp, err := strconv.Atoi(keyvals["exp"])
		if err != nil {
			log.Println(cons.ErrorParsingQueryParameters, err.Error())

			return nil, &model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}

		queryParams.Exp = exp
	}

	if keyvals["dtds"] != "" {
		dtds, err := strconv.Atoi(keyvals["dtds"])
		if err != nil {
			log.Println(cons.ErrorParsingQueryParameters, err.Error())

			return nil, &model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}

		queryParams.DTDs = dtds
	}

	if _, ok := keys["last_played"]; ok {
		queryParams.LastPlayed = ok
	}

	queryParams.ID = keyvals["id"]
	queryParams.Name = keyvals["name"]
	queryParams.Faction = keyvals["faction"]
	queryParams.Race = keyvals["race"]
	queryParams.Region = keyvals["region"]
	queryParams.Class = keyvals["class"]

	return queryParams, nil
}

func getCharactersParams(req *http.Request) (*model.CharacterParams, *model.Response) {
	var reqCharacter model.Character

	if req.Method == "POST" || req.Method == "PATCH" {
		err := json.NewDecoder(req.Body).Decode(&reqCharacter)
		if err != nil {
			log.Println(cons.ErrorParsingBodyParamters, err.Error())

			return nil, &model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}
	}

	return &model.CharacterParams{
		ReqCharacter: &reqCharacter,
	}, nil
}
