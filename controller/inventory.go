package controller

import (
	"encoding/json"
	"hastur/model"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/unrolled/render"

	cons "hastur/constant"
)

type InventoryUseCase interface {
	GetInventories(*model.InventoryQueryParams) model.Response
	PatchGold(*model.InventoryQueryParams) model.Response
	GetInventory(*model.InventoryQueryParams) model.Response
	PostInventory(*model.InventoryQueryParams) model.Response
	DeleteInventory(*model.InventoryQueryParams) model.Response
	PostItems(*model.InventoryParams) model.Response
	PatchItems(*model.InventoryParams) model.Response
	DeleteItems(*model.InventoryQueryParams) model.Response
	DeleteMagicItems(*model.InventoryQueryParams) model.Response
	PatchBankGold(*model.InventoryQueryParams) model.Response
	PatchBankItems(*model.BankingParams) model.Response
	PatchBankMagicItems(*model.BankingParams) model.Response
}

type Inventory struct {
	render  *render.Render
	usecase InventoryUseCase
}

func NewInventory(r *render.Render, u InventoryUseCase) *Inventory {
	return &Inventory{
		render:  r,
		usecase: u,
	}
}

func (c Inventory) GetInventories(w http.ResponseWriter, r *http.Request) {
	params, err := getInventoryQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.GetInventories(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) GetInventory(w http.ResponseWriter, r *http.Request) {
	params, err := getInventoryQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.GetInventory(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) PostInventory(w http.ResponseWriter, r *http.Request) {
	params, err := getInventoryQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PostInventory(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) DeleteInventory(w http.ResponseWriter, r *http.Request) {
	params, err := getInventoryQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.DeleteInventory(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) PatchGold(w http.ResponseWriter, r *http.Request) {
	params, err := getInventoryQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PatchGold(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) PostItems(w http.ResponseWriter, r *http.Request) {
	params, err := getItemParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PostItems(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) PatchItems(w http.ResponseWriter, r *http.Request) {
	params, err := getItemParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PatchItems(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) DeleteItems(w http.ResponseWriter, r *http.Request) {
	params, err := getInventoryQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.DeleteItems(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) DeleteMagicItems(w http.ResponseWriter, r *http.Request) {
	params, err := getInventoryQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.DeleteMagicItems(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) PatchBankGold(w http.ResponseWriter, r *http.Request) {
	params, err := getInventoryQueryParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PatchBankGold(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) PatchBankItems(w http.ResponseWriter, r *http.Request) {
	params, err := getBankingParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PatchBankItems(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c Inventory) PatchBankMagicItems(w http.ResponseWriter, r *http.Request) {
	params, err := getBankingParams(r)
	if err != nil {
		log.Println(cons.ErrorParsingQueryParameters, err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.PatchBankMagicItems(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func getInventoryQueryParams(req *http.Request) (*model.InventoryQueryParams, *model.Response) {
	keys := req.URL.Query()
	nameParam := keys["name"]
	idParam := keys["id"]
	goldParam := keys["gold"]

	queryParams := model.InventoryQueryParams{
		ID:    "",
		Ids:   nil,
		Name:  "",
		Names: nil,
		Gold:  0,
	}

	if len(nameParam) == 1 {
		names := strings.Split(nameParam[0], ",")
		if names[0] != "" {
			queryParams.Name = names[0]
			queryParams.Names = names
		}
	} else if len(nameParam) > 0 {
		queryParams.Name = nameParam[0]
		queryParams.Names = nameParam
	}

	if _, ok := keys["bank"]; ok {
		queryParams.ToBank = ok
	}

	if len(idParam) == 1 {
		ids := strings.Split(idParam[0], ",")
		if ids[0] != "" {
			queryParams.ID = ids[0]
			queryParams.Ids = ids
		}
	} else if len(idParam) > 0 {
		queryParams.ID = idParam[0]
		queryParams.Ids = idParam
	}
	if len(goldParam) > 0 {
		gold, err := strconv.ParseFloat(goldParam[0], 64)
		if err != nil {
			log.Println(err)

			return nil, &model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.ErrorParsingQueryParameters,
			}
		}

		queryParams.Gold = gold
	}

	return &queryParams, nil
}

func getBankingParams(req *http.Request) (*model.BankingParams, *model.Response) {
	keys := req.URL.Query()
	var params model.BankingParams

	if req.Method == "PATCH" {
		err := json.NewDecoder(req.Body).Decode(&params)
		if err != nil {
			log.Println(cons.ErrorParsingBodyParamters, err.Error())

			return nil, &model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}

		if val, ok := keys["id"]; ok {
			params.ID = val[0]
		}
	}

	return &params, nil
}

func getItemParams(req *http.Request) (*model.InventoryParams, *model.Response) {
	keys := req.URL.Query()
	var params model.InventoryParams

	if req.Method == "POST" || req.Method == "PATCH" {
		err := json.NewDecoder(req.Body).Decode(&params)
		if err != nil {
			log.Println(cons.ErrorParsingBodyParamters, err.Error())

			return nil, &model.Response{
				Code:    http.StatusInternalServerError,
				Message: cons.InternalServerError,
			}
		}
	}

	if val, ok := keys["id"]; ok {
		params.ID = val[0]
	}

	return &params, nil
}
