package controller

import (
	"encoding/json"
	"hastur/model"
	"log"
	"net/http"

	"github.com/unrolled/render"

	cons "hastur/constant"
)

type UserUsecase interface {
	Signup(params *model.UserParams) model.Response
	Login(params *model.UserParams) model.Response
}

// User controller struct
type User struct {
	render  *render.Render
	usecase UserUsecase
}

// NewUser controller
func NewUser(r *render.Render, u UserUsecase) *User {
	return &User{
		render:  r,
		usecase: u,
	}
}

// Login handler function
func (c *User) Login(w http.ResponseWriter, r *http.Request) {
	params, err := getUserParams(r)
	if err != nil {
		log.Println(err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.Login(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func (c *User) Signup(w http.ResponseWriter, r *http.Request) {
	params, err := getUserParams(r)
	if err != nil {
		log.Println(err)
		c.render.JSON(w, err.Code, err.Message)

		return
	}
	resp := c.usecase.Signup(params)
	c.render.JSON(w, resp.Code, resp.Message)
}

func getUserParams(req *http.Request) (*model.UserParams, *model.Response) {
	var params model.UserParams

	err := json.NewDecoder(req.Body).Decode(&params)
	if err != nil {
		return nil, &model.Response{
			Code:    http.StatusInternalServerError,
			Message: cons.InternalServerError,
		}
	}

	return &params, nil
}
