package controller

import (
	"net/http"

	"github.com/unrolled/render"
)

// Healther to check dephts of health
type Healther interface {
	IsHealthy() error
}

type HealthChecker []Healther

// Status controller struct
type Status struct {
	render   *render.Render
	healther HealthChecker
}

// NewStatus Controller
func NewStatus(r *render.Render, h HealthChecker) *Status {
	return &Status{
		render:   r,
		healther: h,
	}
}

// Healhz checks external dependencies status
func (c *Status) Healthz(w http.ResponseWriter, req *http.Request) {
	if err := c.healther.IsHealthy(); err != nil {
		c.render.Text(w, http.StatusServiceUnavailable, err.Error())
		return
	}

	c.render.Text(w, http.StatusOK, "Succesfull death saving throw")
}

func (h HealthChecker) IsHealthy() error {
	for i := range h {
		if err := h[i].IsHealthy(); err != nil {
			return err
		}
	}
	return nil
}
