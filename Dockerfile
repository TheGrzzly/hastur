FROM golang:latest

RUN mkdir -p /go/src/hastur

WORKDIR /go/src/hastur

COPY go.mod .

COPY go.sum .

RUN go mod download

COPY . .

ENV PORT=1234

RUN go build

CMD [ "./hastur" ]

EXPOSE 1234