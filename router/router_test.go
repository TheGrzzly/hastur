package router

import (
	"hastur/router/mocks"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestSetup(t *testing.T) {
	testCases := []struct {
		desc                string
		handlerName         string
		handlerMethod       string
		status              int
		expectStatusCall    bool
		expectCharacterCall bool
		expectFactionCall   bool
		expectInventoryCall bool
		expectUserCall      bool
		expectJWTValidation bool
	}{
		{
			desc:             "/healthz",
			handlerName:      "Healthz",
			handlerMethod:    "Get",
			expectStatusCall: true,
			status:           http.StatusOK,
		},
		{
			desc:           "/login",
			handlerName:    "Login",
			handlerMethod:  "Post",
			expectUserCall: true,
			status:         http.StatusOK,
		},
		{
			desc:           "/signup",
			handlerName:    "Signup",
			handlerMethod:  "Post",
			expectUserCall: true,
			status:         http.StatusOK,
		},
		{
			desc:              "/factions",
			handlerName:       "GetFactions",
			handlerMethod:     "GetAll",
			expectFactionCall: true,
			status:            http.StatusOK,
		},
		{
			desc:              "/faction",
			handlerName:       "GetFaction",
			handlerMethod:     "GetSingle",
			expectFactionCall: true,
			status:            http.StatusOK,
		},
		{
			desc:                "/faction",
			handlerName:         "PostFaction",
			handlerMethod:       "Post",
			expectFactionCall:   true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/faction",
			handlerName:         "PatchFaction",
			handlerMethod:       "Patch",
			expectFactionCall:   true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/faction",
			handlerName:         "DeleteFaction",
			handlerMethod:       "Delete",
			expectFactionCall:   true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/characters",
			handlerName:         "GetCharacters",
			handlerMethod:       "GetAll",
			expectCharacterCall: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/characters",
			handlerName:         "PostCharacter",
			handlerMethod:       "Post",
			expectCharacterCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/characters",
			handlerName:         "PatchCharacter",
			handlerMethod:       "Patch",
			expectCharacterCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/characters",
			handlerName:         "DeleteCharacter",
			handlerMethod:       "Delete",
			expectCharacterCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/inventories",
			handlerName:         "GetInventories",
			handlerMethod:       "GetAll",
			expectInventoryCall: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/inventory",
			handlerName:         "GetInventory",
			handlerMethod:       "GetSingle",
			expectInventoryCall: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/inventory",
			handlerName:         "PostInventory",
			handlerMethod:       "Post",
			expectInventoryCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/inventory",
			handlerName:         "DeleteInventory",
			handlerMethod:       "Delete",
			expectInventoryCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/gold",
			handlerName:         "PatchGold",
			handlerMethod:       "Patch",
			expectInventoryCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/items",
			handlerName:         "PostItems",
			handlerMethod:       "Post",
			expectInventoryCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/items",
			handlerName:         "PatchItems",
			handlerMethod:       "Patch",
			expectInventoryCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/items",
			handlerName:         "DeleteItems",
			handlerMethod:       "Delete",
			expectInventoryCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:                "/magic_items",
			handlerName:         "DeleteMagicItems",
			handlerMethod:       "Delete",
			expectInventoryCall: true,
			expectJWTValidation: true,
			status:              http.StatusOK,
		},
		{
			desc:   "/badrequest",
			status: http.StatusNotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			statusController := &mocks.StatusController{}
			userController := &mocks.UserController{}
			characterController := &mocks.CharacterController{}
			inventoryController := &mocks.InventoryController{}
			factionController := &mocks.FactionController{}
			jwtMiddelware := &mocks.JWTMiddelware{}

			switch {
			case tc.expectStatusCall:
				statusController.On(tc.handlerName, mock.Anything, mock.Anything).
					Run(func(args mock.Arguments) {
						args[0].(http.ResponseWriter).WriteHeader(http.StatusOK)
					})
			case tc.expectCharacterCall:
				characterController.On(tc.handlerName, mock.Anything, mock.Anything).
					Run(func(args mock.Arguments) {
						args[0].(http.ResponseWriter).WriteHeader(http.StatusOK)
					})
			case tc.expectFactionCall:
				factionController.On(tc.handlerName, mock.Anything, mock.Anything).
					Run(func(args mock.Arguments) {
						args[0].(http.ResponseWriter).WriteHeader(http.StatusOK)
					})
			case tc.expectInventoryCall:
				inventoryController.On(tc.handlerName, mock.Anything, mock.Anything).
					Run(func(args mock.Arguments) {
						args[0].(http.ResponseWriter).WriteHeader(http.StatusOK)
					})
			case tc.expectUserCall:
				userController.On(tc.handlerName, mock.Anything, mock.Anything).
					Run(func(args mock.Arguments) {
						args[0].(http.ResponseWriter).WriteHeader(http.StatusOK)
					})
			}

			req, err := http.NewRequest("GET", tc.desc, nil)

			switch tc.handlerMethod {
			case "GetAll":
				req, err = http.NewRequest("GET", tc.desc, nil)
			case "Post":
				req, err = http.NewRequest("POST", tc.desc, nil)
			case "Patch":
				req, err = http.NewRequest("PATCH", tc.desc, nil)
			case "Delete":
				req, err = http.NewRequest("DELETE", tc.desc, nil)
			}

			if err != nil {
				t.Fatal(err)
			}

			rr := httptest.NewRecorder()

			var arg http.HandlerFunc
			arg = nil

			switch tc.handlerName {
			case "PostFaction":
				arg = factionController.PostFaction
			case "PatchFaction":
				arg = factionController.PatchFaction
			case "DeleteFaction":
				arg = factionController.DeleteFaction
			case "PostCharacter":
				arg = characterController.PostCharacter
			case "PatchCharacter":
				arg = characterController.PatchCharacter
			case "DeleteCharacter":
				arg = characterController.DeleteCharacter
			case "PostInventory":
				arg = inventoryController.PostInventory
			case "DeleteInventory":
				arg = inventoryController.DeleteInventory
			case "GetInventory":
				arg = inventoryController.GetInventory
			case "PatchGold":
				arg = inventoryController.PatchGold
			case "PostItems":
				arg = inventoryController.PostItems
			case "PatchItems":
				arg = inventoryController.PatchItems
			case "DeleteItems":
				arg = inventoryController.DeleteItems
			case "DeleteMagicItems":
				arg = inventoryController.DeleteMagicItems
			}

			if tc.expectJWTValidation {
				jwtMiddelware.On("ValidateJWTAdmin", mock.AnythingOfType("http.HandlerFunc")).Return(arg)
			} else {
				jwtMiddelware.On("ValidateJWTAdmin", mock.Anything).Return(nil)
			}

			r := GetRouter(statusController, userController, factionController, characterController, inventoryController, jwtMiddelware)

			r.ServeHTTP(rr, req)

			statusController.AssertExpectations(t)
			userController.AssertExpectations(t)
			factionController.AssertExpectations(t)
			characterController.AssertExpectations(t)
			inventoryController.AssertExpectations(t)
			jwtMiddelware.AssertExpectations(t)

			assert.Equal(t, tc.status, rr.Code)
		})
	}
}
