package router

import (
	"net/http"

	"github.com/gorilla/mux"
)

// StatusController with the handler methods relevant to the router
type StatusController interface {
	Healthz(w http.ResponseWriter, req *http.Request)
}

// User controller with the handler methods relevant to the router
type UserController interface {
	Login(w http.ResponseWriter, req *http.Request)
	Signup(w http.ResponseWriter, req *http.Request)
}

type FactionController interface {
	GetFactions(w http.ResponseWriter, req *http.Request)
	GetFaction(w http.ResponseWriter, req *http.Request)
	PostFaction(w http.ResponseWriter, req *http.Request)
	PatchFaction(w http.ResponseWriter, req *http.Request)
	DeleteFaction(w http.ResponseWriter, req *http.Request)
}

type CharacterController interface {
	GetCharacters(w http.ResponseWriter, req *http.Request)
	PostCharacter(w http.ResponseWriter, req *http.Request)
	PatchCharacter(w http.ResponseWriter, req *http.Request)
	DeleteCharacter(w http.ResponseWriter, req *http.Request)
	GetDowntimeDays(w http.ResponseWriter, req *http.Request)
	PatchDowntimeDays(w http.ResponseWriter, req *http.Request)
	PostDowntimeDays(w http.ResponseWriter, req *http.Request)
	GetExperience(w http.ResponseWriter, req *http.Request)
	PatchExperience(w http.ResponseWriter, req *http.Request)
}

type InventoryController interface {
	GetInventories(w http.ResponseWriter, req *http.Request)
	PatchGold(w http.ResponseWriter, req *http.Request)
	GetInventory(w http.ResponseWriter, req *http.Request)
	PostInventory(w http.ResponseWriter, req *http.Request)
	DeleteInventory(w http.ResponseWriter, req *http.Request)
	PostItems(w http.ResponseWriter, req *http.Request)
	PatchItems(w http.ResponseWriter, req *http.Request)
	DeleteItems(w http.ResponseWriter, req *http.Request)
	DeleteMagicItems(w http.ResponseWriter, req *http.Request)
	PatchBankGold(w http.ResponseWriter, req *http.Request)
	PatchBankItems(w http.ResponseWriter, req *http.Request)
	PatchBankMagicItems(w http.ResponseWriter, req *http.Request)
}

type JWTMiddelware interface {
	ValidateJWTAdmin(handler http.HandlerFunc) http.HandlerFunc
	ValidateJWTDev(handler http.HandlerFunc) http.HandlerFunc
}

// GetRouter
func GetRouter(sc StatusController, uc UserController, fc FactionController, cc CharacterController, ic InventoryController, jm JWTMiddelware) *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/healthz", sc.Healthz).Methods(http.MethodGet).Name("healthz")

	r.HandleFunc("/login", uc.Login).Methods(http.MethodPost).Name("login")
	r.HandleFunc("/signup", uc.Signup).Methods(http.MethodPost).Name("signup")

	r.HandleFunc("/factions", fc.GetFactions).Methods(http.MethodGet).Name("getFactions")
	r.HandleFunc("/faction", jm.ValidateJWTAdmin(fc.PostFaction)).Methods(http.MethodPost).Name("postFaction")
	r.HandleFunc("/faction", fc.GetFaction).Methods(http.MethodGet).Name("getFaction")
	r.HandleFunc("/faction", jm.ValidateJWTAdmin(fc.PatchFaction)).Methods(http.MethodPatch).Name("patchFaction")
	r.HandleFunc("/faction", jm.ValidateJWTAdmin(fc.DeleteFaction)).Methods(http.MethodDelete).Name("deleteFaction")

	r.HandleFunc("/characters", cc.GetCharacters).Methods(http.MethodGet).Name("getCharacters")
	r.HandleFunc("/characters", jm.ValidateJWTAdmin(cc.PostCharacter)).Methods(http.MethodPost).Name("postCharacter")
	r.HandleFunc("/characters", jm.ValidateJWTAdmin(cc.PatchCharacter)).Methods(http.MethodPatch).Name("patchCharacter")
	r.HandleFunc("/characters", jm.ValidateJWTAdmin(cc.DeleteCharacter)).Methods(http.MethodDelete).Name("deleteCharacter")

	r.HandleFunc("/dtds", cc.GetDowntimeDays).Methods(http.MethodGet).Name("getDownTimeDays")
	r.HandleFunc("/dtds", jm.ValidateJWTAdmin(cc.PatchDowntimeDays)).Methods(http.MethodPatch).Name("patchDownTimeDays")
	r.HandleFunc("/dtds", jm.ValidateJWTAdmin(cc.PostDowntimeDays)).Methods(http.MethodPost).Name("postDownTimeDays")

	r.HandleFunc("/exp", cc.GetExperience).Methods(http.MethodGet).Name("getExperience")
	r.HandleFunc("/exp", jm.ValidateJWTAdmin(cc.PatchExperience)).Methods(http.MethodPatch).Name("patchExperience")

	r.HandleFunc("/inventories", ic.GetInventories).Methods(http.MethodGet).Name("getInventories")

	r.HandleFunc("/inventory", ic.GetInventory).Methods(http.MethodGet).Name("getInventory")
	r.HandleFunc("/inventory", jm.ValidateJWTAdmin(ic.PostInventory)).Methods(http.MethodPost).Name("postInventory")
	r.HandleFunc("/inventory", jm.ValidateJWTAdmin(ic.DeleteInventory)).Methods(http.MethodDelete).Name("deleteInventory")

	r.HandleFunc("/gold", jm.ValidateJWTAdmin(ic.PatchGold)).Methods(http.MethodPatch).Name("patchGold")

	r.HandleFunc("/bank_gold", jm.ValidateJWTAdmin(ic.PatchBankGold)).Methods(http.MethodPatch).Name("patchBankGold")

	r.HandleFunc("/items", jm.ValidateJWTAdmin(ic.PostItems)).Methods(http.MethodPost).Name("postItems")
	r.HandleFunc("/items", jm.ValidateJWTAdmin(ic.PatchItems)).Methods(http.MethodPatch).Name("patchItems")
	r.HandleFunc("/items", jm.ValidateJWTAdmin(ic.DeleteItems)).Methods(http.MethodDelete).Name("deleteItems")
	r.HandleFunc("/magic_items", jm.ValidateJWTAdmin(ic.DeleteMagicItems)).Methods(http.MethodDelete).Name("deleteMagicItems")

	r.HandleFunc("/bank_items", jm.ValidateJWTAdmin(ic.PatchBankItems)).Methods(http.MethodPatch).Name("patchBankItems")
	r.HandleFunc("/bank_magic_items", jm.ValidateJWTAdmin(ic.PatchBankMagicItems)).Methods(http.MethodPatch).Name("patchBankMagicItems")

	return r
}
