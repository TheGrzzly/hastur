module hastur

go 1.16

require (
	github.com/aws/aws-sdk-go v1.40.5
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.7.0
	github.com/unrolled/render v1.1.0
	golang.org/x/crypto v0.0.0-20210506145944-38f3c27a63bf
)
